/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/


#include <stdio.h>
#include <limits.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>

#include <cuda_runtime.h>
#include <cuda.h>

#include "mcl.h"
#include "common.h"
#include "config.h"
#include "comm_cmpl.h"
#include "cmnd_engine.h"

/*

*/
struct pci_topo {
/*
--File format, file is string based--
"PCIe TOPOLOGY" : STRING  magic "number"
total number of devices:INT
array of devices: G=GPU,S=SWITCH,C=CPU
2-dim INT array of topolgy, 0 no connection 1 edge bettwen device. index from previus array, 0,1,2.. . 
*/
    char *pci_topo_string;
    int device_count;
    int gpu_count;
    int switch_count;
    int cpu_count; /* in the topology not on system */

};


/*

*/
struct mcl_common {
    
    /* config */
    char pci_topo_filename[CNF_MAXLEN_PCI_TOPO_FM]; 

    /* GPU attributs */
    int gpu_count; 
    int cpu_count; //1 
    struct pci_topo pci_topo;

    /*Standard comm compiler */
    comm_cmpl_func_t comm_cmpl;

    /* Default MCL streams */
    cudaStream_t *up_streams;
    cudaStream_t *down_streams;

};
static struct mcl_common common; 

#include "pttr.h"

/* Function declerations */
static mcl_res_t _mcl_alloc_pttr(struct comm_pttr **comm_pt,const comm_cmpl_func_t cmpl,const bool is_const,const bool is_predef); 


mcl_res_t mcl_sync_excomm(struct comm_pttr *pt) {

    DEBUG_PRINT("Entry mcl_sync_excomm\n");

    mcl_res_t err = MCL_SUCCESS;
    /* 
        Same signature as last time, assertion uphold by functions updating states in
        in this struct
    */
    if(!(pt->is_same)) {
        /* Compile the command list */
        comm_cmpl_func_t cmpl = pt->cmpl;
        DEBUG_PRINT("cmpl func %p\n",cmpl);
        err = (*cmpl)(pt);
        RETURN_ERROR(err);
    }
    /* Run the commands */
    DEBUG_PRINT("Calling the command engine!!\n");
    err = run_cmnd_engine(pt);
    RETURN_ERROR(err);     

    return MCL_SUCCESS;
ERROR_RETURN:
    DEBUG_PRINT("Error return:%d\n",err);
    return err;
}



mcl_res_t mcl_cng_comm_cmpl(mcl_comm_cmpl_t cc) {

    return cmpl_select(&(common.comm_cmpl),cc);    
}


/*  Parser for the config file */
mcl_res_t prs_cnf(struct mcl_common *cm) {
    static const char cnf_file[] = CNF_CNFFILE;    

    /* Open config file */
    FILE * fp = fopen(cnf_file,"r");
    if(!fp) {
        fprintf(stderr,"%s: %s\n",cnf_file,strerror(errno));
        return MCL_ERROR;
    }

    /* Parse config file */
    char id[CNF_MAXLEN_ID], value[CNF_MAXLEN_VALUE];
    memset(id,0,sizeof(id));    
    memset(value,0,sizeof(value));
    /* File name of PCI topology */
    int err = fscanf(fp,"%s %s\n",id,value);
    if(!err) {
        perror("fscan");
        fclose(fp);//We do not check for error
        return MCL_ERROR;
    }    
    strncpy(cm->pci_topo_filename,value,sizeof(value));

    /* Close the file */
    err = fclose(fp);
    if(err) {
        perror("fclose");
        return MCL_ERROR;
    }
    
    return MCL_SUCCESS;

}
/* Parser for the pci topology file */
static mcl_res_t prs_pci_topo(char filenm[CNF_MAXLEN_PCI_TOPO_FM],struct pci_topo *pci_topo) {
    
    const int gpu_count = common.gpu_count;
    const int cpu_count = common.cpu_count;
    //const int device_count = common.cpu_count;

    /* 
        TODO:  
        -parse the topo file
        -assert values agaings values in common, return error if wrong
         use the MCL_INVALID_VALUE_PCI_TOPO_FILE
    */     
    
    // TODO This is a tmp solution
    pci_topo->gpu_count = gpu_count;
    pci_topo->cpu_count = cpu_count;
    pci_topo->device_count = pci_topo->cpu_count + pci_topo->gpu_count; 
 
    //This is false now... 
    if(pci_topo->gpu_count != gpu_count || pci_topo->cpu_count != cpu_count) 
        return MCL_INVALID_VALUE_PCI_TOPO_FILE;
 
 
    return MCL_SUCCESS;
}

static void free_pci_topo(void) {
    ;
}


mcl_res_t mcl_init(void) {

    /* Parse the config file */    
    mcl_res_t err = prs_cnf(&common);
    RETURN_ERROR(err);    

    /* Set common struct */
    int gpu_count;
    CUDA_ERROR(cudaGetDeviceCount(&gpu_count),"cudaGetDevice");
    common.gpu_count = gpu_count;
    common.cpu_count = CNF_MAX_CPU;
    
    /* Set the default communication compiler */
    err = cmpl_select(&(common.comm_cmpl),MCL_COMM_CMPL_ALL); //TODO: set cmpl from cfg    
    RETURN_ERROR(err);
    
    /* Parse PCIe topology file */
    err = prs_pci_topo(common.pci_topo_filename,&common.pci_topo);
    RETURN_ERROR(err);
 

    /* Make up and down streams */
    /* Alternative is to have theme in pttr struct, but not sure hove many streams
        can be created/suported. So make it centraly, and use it for syncron opt. 
    */
    cudaStream_t *up_streams = (cudaStream_t *)malloc(sizeof(cudaStream_t)*gpu_count);
    cudaStream_t *down_streams = (cudaStream_t *)malloc(sizeof(cudaStream_t)*gpu_count);
    for(int i =0;i<gpu_count; i++) {
        fprintf(stderr,"stream_create!!!\n");;
        CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
        CUDA_ERROR(cudaStreamCreateWithFlags(&up_streams[i],cudaStreamNonBlocking),"cudaStreamCreate");
        CUDA_ERROR(cudaStreamCreateWithFlags(&down_streams[i],cudaStreamNonBlocking),"cudaStreamCreate");
        
    }
    common.up_streams = up_streams;
    common.down_streams = down_streams;
    
    /* Enable peer acces */
    for(int i = 0; i < gpu_count; i++) {
        CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
	    for(int j = (i+1)%gpu_count; j != i; j= (j+1)%gpu_count) {
        	CUDA_ERROR(cudaDeviceEnablePeerAccess(j,0),"cudaDeviceEnablePeerAcces");
    	}
    } 

    return MCL_SUCCESS; 

ERROR_CUDA:
    return MCL_CUDA_ERROR;
ERROR_RETURN:
    return err;
}

mcl_res_t mcl_finalize() {
    //mcl_res_t err = MCL_SUCCESS;
    
    /* Free the PCIe topology*/
    free_pci_topo();

    const int gpu_count = common.gpu_count;

    /* Free the default MCL streams */ 
    cudaStream_t *up_streams = common.up_streams;
    cudaStream_t *down_streams = common.down_streams;
    for(int i =0;i<gpu_count; i++) {
        CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
        CUDA_ERROR(cudaStreamDestroy(up_streams[i]),"cudaStreamDestroy");
        CUDA_ERROR(cudaStreamDestroy(down_streams[i]),"cudaStreamDestroy");
    }
    

    return MCL_SUCCESS; //TODO, more stuff!

ERROR_CUDA:
    return MCL_CUDA_ERROR;
}

#include "pttr_opt.h"
/*
    Allocs a communication pattern 
    
*/

mcl_res_t mcl_alloc_pttr(struct comm_pttr **comm_pt,const uint32_t opt) {
      
    /* Decode opt */
    bool is_const = false;
    if(opt) {
        if(PTTR_OPT_GET_TYPE_CONST(opt)) 
            is_const = true;
        //...
    }
    
    return _mcl_alloc_pttr(comm_pt,common.comm_cmpl,is_const,false);

}

mcl_res_t mcl_alloc_bcast(void *src,size_t size,mcl_pttr_t **pt,const int dst_cnt,...) {
    
    va_list list;

    struct comm_pttr *cp;
    mcl_res_t err = _mcl_alloc_pttr(&cp,common.comm_cmpl,true,true);//TODO add bcast cmpl
    RETURN_ERROR(err);
    
    va_start(list,dst_cnt);
    for(int i = 0; i<dst_cnt; i++) {
        err = mcl_add_path(va_arg(list,void*),src,size,cp);      
        RETURN_ERROR(err);
    }
    va_end(list);


    return MCL_SUCCESS;
ERROR_RETURN:
    return err;
}
mcl_res_t mcl_alloc_scatter(void *src,size_t size,mcl_pttr_t **pt,const int dst_cnt,...) {

    va_list list;

    struct comm_pttr *cp;
    mcl_res_t err = _mcl_alloc_pttr(&cp,common.comm_cmpl,true,true);//TODO add bcast cmpl
    RETURN_ERROR(err);
    
    va_start(list,dst_cnt);
    for(int i = 0; i<dst_cnt; i++) {
        err = mcl_add_path(va_arg(list,void*),(uintptr_t)(src)+i*size,size,cp);     
        RETURN_ERROR(err);
    }
    va_end(list);

    *pt = cp;

    return MCL_SUCCESS;
ERROR_RETURN:
    return err;
 
   
}

mcl_res_t mcl_alloc_gather(void *dst,size_t size,mcl_pttr_t **pt,const int src_cnt,...) {
  
    va_list list;

    struct comm_pttr *cp;
    mcl_res_t err = _mcl_alloc_pttr(&cp,common.comm_cmpl,true,true);//TODO add bcast cmpl
    RETURN_ERROR(err);
    
    va_start(list,src_cnt);
    for(int i = 0; i<src_cnt; i++) {
        err = mcl_add_path((uintptr_t)(dst)+i*size,va_arg(list,void*),size,cp);     
        RETURN_ERROR(err);
    }
    va_end(list);

    *pt = cp;


    return MCL_SUCCESS;
ERROR_RETURN:
    return err;   
}

mcl_res_t mcl_alloc_1dim_bexch(size_t size,mcl_pttr_t **pt,int cnt,...) {

    va_list list;

    struct comm_pttr *cp;
    mcl_res_t err = _mcl_alloc_pttr(&cp,common.comm_cmpl,true,true);//TODO add bcast cmpl
    RETURN_ERROR(err);
    
    va_start(list,cnt);
    for(int i = 0; i<cnt; i++) {
        const void *dst = va_arg(list,void*);
        const void *src = va_arg(list,void*);
        err = mcl_add_path(dst,src,size,cp);     
        RETURN_ERROR(err);
    }
    va_end(list);

    cmpl_select(cp->cmpl,MCL_COMM_CMPL_PAIR); //TODO make this selectable from config. 

    *pt = cp;

    return MCL_SUCCESS;
ERROR_RETURN:
    return err;   
}



/* Internal version */
static mcl_res_t _mcl_alloc_pttr(struct comm_pttr **comm_pt,const comm_cmpl_func_t cmpl,const bool is_const,const bool is_predef) {
    

    /* Calculate node count */
    const int cmp_unit_cnt = common.gpu_count + common.cpu_count;
    const int node_count = cmp_unit_cnt*cmp_unit_cnt;
    DEBUG_PRINT("node_count: %d\n",node_count);   
 
    /* Alloc pattern */
    struct comm_pttr *cp = (struct comm_pttr *)malloc(sizeof(struct comm_pttr));
    memset(cp,0,sizeof(struct comm_pttr)); 
    
    /* Alloc nodes 
        comm_topo is an cmp_unit_cnt X cmp_unit_cnt 2-dim array 
        of struct comm_topo_node * . A element is a path from its y index to it's x index
        If there is no path from a node it is NULL
        If there is a path, it points to it's linearised index element in nodes. 
        There should be now path from element 0,0 1,1 2,2 etc.  
    */
    cp->comm_topo=(struct comm_topo_node **)malloc(sizeof(struct comm_topo_node*)*node_count);
    memset(cp->comm_topo,0,sizeof(struct comm_topo_node*)*node_count);
    
    cp->nodes=(struct comm_topo_node *)malloc(sizeof(struct comm_topo_node)*node_count);
    memset(cp->nodes,0,sizeof(struct comm_topo_node)*node_count);
    // TODO: make a common memory pool ?? 
    
    /* Init all elements in cp->nodes  */
    for(int i = 0; i<node_count; i++) {
        ((cp->nodes)[i].top_sentinal).is_sentinal = ((cp->nodes)[i].bottom_sentinal).is_sentinal = true; //For debug
        (cp->nodes)[i].top_sentinal.prv = &((cp->nodes)[i].bottom_sentinal);
        (cp->nodes)[i].bottom_sentinal.nxt = &((cp->nodes)[i].top_sentinal);
        (cp->nodes)[i].stck =  &((cp->nodes)[i].top_sentinal);
    }   

    /*Set comm_pttr_type_t */
    for(int j = 0; j< cmp_unit_cnt; j++ ) 
        for(int i = 0; i < cmp_unit_cnt; i++) {
            const size_t index = i + j*cmp_unit_cnt;
            if(i < cmp_unit_cnt -1 && j < cmp_unit_cnt -1 )
                (cp->nodes)[index].type  = COMM_PTTR_GPU_GPU; 
            else if(j < cmp_unit_cnt -1) 
                (cp->nodes)[index].type  = COMM_PTTR_GPU_CPU;
            else
                (cp->nodes)[index].type  = COMM_PTTR_CPU_GPU;     
        }
    


    /* Set values in this comm_pttr */ 
    cp->comm_topo_node_count = node_count;
    cp->comm_topo_x_dim = cp->comm_topo_y_dim = cmp_unit_cnt;
    cp->pci_topo = &common.pci_topo;
    /* The compiler used */
    cp->cmpl = cmpl; 
    /* Set gpu count */
    cp->gpu_count = common.gpu_count;
    /* Set the sync default up and down streams */
    cp->up_streams = common.up_streams;
    cp->down_streams = common.down_streams;

    /* Init the cmnd 
        making linked list */
    cp->prg =  cp->_mem_cmnd;
    struct cmnd *cmnd = cp->prg;
    for(int i = 1; i < CNF_CMND_MAX_PRALLOC_COUNT; i++) {
        cmnd->nxt = &cp->_mem_cmnd[i];
        cmnd = cmnd->nxt;
    }
    cmnd->nxt = NULL;


    /* return the comm_pttr struct */
    *comm_pt = cp;
 
    return MCL_SUCCESS;
}


mcl_res_t mcl_free_pttr(struct comm_pttr *cp) {
    free(cp->comm_topo);
    free(cp->nodes);
    free(cp);
    return MCL_SUCCESS;
}


static mcl_res_t push_path(void *dst,int dst_dev,void *src,int src_dev,size_t size,mcl_pttr_t * pt) {
    
    DEBUG_PRINT("Entry:dst_dev:%d:src_dev:%d:size:%d:src:%p:dst:%p\n",dst_dev,src_dev,size,src,dst);
    
    /* Extract struct */
    const int comm_topo_x_dim = pt->comm_topo_x_dim;
    struct comm_topo_node ** comm_topo = pt->comm_topo;
    struct comm_topo_node *nodes = pt->nodes;
    
    const int index = dst_dev + src_dev*comm_topo_x_dim;
    
    struct comm_topo_node **node = &comm_topo[index];
   
    /* If there is no path, add node from index */ 
    if(!(*node)) {
        *node = &nodes[index];
    }
    
    /* Alloc communication path ,set values */
    struct comm_path *cp;
    mcl_res_t err = comm_path_alloc(src,dst,size,&cp);
    DEBUG_PRINT("index:%d:cp->src:%p:cp->dst:%p:size:%u\n",index,cp->src,cp->dst,cp->size);    
     
    /* 
    Push this path onto the node stack  
    -assertion: first element is top_sentinal, last element is bottom_sentinal
    */
    assert((*node)->stck == &((*node)->top_sentinal));
    cp->nxt = (*node)->stck; 
    cp->prv = ((*node)->stck)->prv;
    ((*node)->stck)->prv->nxt = cp;
    ((*node)->stck)->prv = cp;
    (*node)->count++; //Not needed
    DEBUG_PRINT("node->count: %d\n",(*node)->count);

    return MCL_SUCCESS;
}

static mcl_res_t pop_path(void **dst,void **src,size_t *size,int dst_dev,int src_dev,mcl_pttr_t *pt) {
    
    const int comm_topo_x_dim = pt->comm_topo_x_dim;
    struct comm_topo_node ** comm_topo = pt->comm_topo;
   // struct comm_topo_node *nodes = pt->nodes;
    //unused
    const int index = dst_dev + src_dev*comm_topo_x_dim;
    
    struct comm_topo_node *node = comm_topo[index];
    if(!node || !node->count ) 
        return MCL_ERROR;
    
    struct comm_path *cp = node->stck->prv;
    *src = cp->src;
    *dst = cp->dst;
    *size = cp->size;
    
    /* remove the path from the node stack */
    cp->prv->nxt = node->stck;
    node->stck->prv = cp->prv;    
    comm_path_free(cp); //TODO: Or return cp ?

    node->tsize -= *size;
    /* If no more paths, remove the node from the communication topology
        preservering the assertion count == 0 -> comm_topo[index] == NULL
    */
    if(!(--(node->count)))  {
        comm_topo_node_zero(node); 
        comm_topo[index] = NULL;
    }
    
    return MCL_SUCCESS;
}

/*
Delites path with given signature, 
Do  linear search.
*/
static mcl_res_t del_path(void *dst,int dst_dev,void *src,int src_dev,struct comm_pttr *pt) {
    
    DEBUG_PRINT("dst_dev:%d src_dev:%d\n",dst_dev,src_dev);
    DEBUG_PRINT("dst:%p src:%p\n",dst,src); 
    /* extract struct comm_pttr*/
    const int comm_topo_x_dim = pt->comm_topo_x_dim;
    struct comm_topo_node ** comm_topo = pt->comm_topo;
    
    /* Get the comm_topo_node node */
    const int index = dst_dev + src_dev*comm_topo_x_dim;
    DEBUG_PRINT("index %d\n",index);    
    struct comm_topo_node *node = comm_topo[index];
    DEBUG_PRINT("node 0x%p\n",node);   
    
     
    /* Return if the node do not exist */
    if(!node) 
        return MCL_UNKNOWN_PATH_SIGNATURE;
    assert(node->count != 0);

    DEBUG_PRINT("node->count %d\n",node->count);
    assert(node->stck == &(node->top_sentinal));
    for(struct comm_path *curr = node->stck->prv; curr != &(node->bottom_sentinal);) {
        DEBUG_PRINT("curr->dst:%p curr->src:%p is_sentinal%d\n",curr->dst,curr->src,curr->is_sentinal);
        if(curr->dst == dst && curr->src == src) {//TODO: Also test for size ?
            /* Extract it from the stck */
            curr->nxt->prv = curr->prv;
            curr->prv->nxt = curr->nxt;
            /* Free the memory */
            comm_path_free(curr);
            /* If no more paths, remove the node from the communication topology
                preservering the assertion count == 0 -> comm_topo[index] == 0
            */
            if(!(--(node->count))) {
                DEBUG_PRINT("node->count == 0,index:%d \n",index);
                comm_topo_node_zero(node);
                comm_topo[index] = NULL;
            }
            DEBUG_PRINT("node->stck 0x%p \n",node->stck);
            return MCL_SUCCESS;
        }
        curr = curr->prv;
    }
    
       
    return MCL_UNKNOWN_PATH_SIGNATURE;
} 

static int parse_cudaAtt(struct cudaPointerAttributes *att,struct comm_pttr *pt) {
    const int gpu_count = common.gpu_count;
    const int cpu_count = common.cpu_count;
    assert(cpu_count == 1); //We do not handel this
    switch(att->memoryType)   {
        case cudaMemoryTypeHost:
            return gpu_count; /* Count is +1 over largest gpu index */  
        case cudaMemoryTypeDevice:
            assert(att->device >= 0 && att->device < gpu_count); //somthing is wrong
            return att->device;
        default:
            assert(false);
            return -1; //Happy compiler
    } 
} 


#define DEBUG_ADD_PATH 1
mcl_res_t mcl_add_path(void *dst,void *src,size_t size,mcl_pttr_t *pt) {

    
    struct cudaPointerAttributes dst_att;
    memset(&dst_att,0,sizeof(dst_att));
    CUDA_ERROR(cudaPointerGetAttributes(&dst_att,dst),"cudaPointerGetAttribute");
    const int dst_dev = parse_cudaAtt(&dst_att,pt);
    
    struct cudaPointerAttributes src_att;
    memset(&src_att,0,sizeof(src_att));
    CUDA_ERROR(cudaPointerGetAttributes(&src_att,src),"cudaPointerGetAttribute");
    const int src_dev = parse_cudaAtt(&src_att,pt);
    DEBUG_PRINT("dst_dev:%d:src_dev:%d:dst:%p:src:%p\n",dst_dev,src_dev,dst,src);
   
    /* Something added, program has to be recompiled (her if push_path fails, better safe then sorry!)*/
    pt->is_same = false;

    /* Make a path add it to pt */
    push_path(dst,dst_dev,src,src_dev,size,pt);

  
    return MCL_SUCCESS;

ERROR_CUDA:
    return MCL_CUDA_ERROR;
     
}
#undef DEBUG_ADD_PATH

mcl_res_t mcl_rmv_path(void *dst,void *src,size_t size,mcl_pttr_t *pt) {  

    DEBUG_PRINT("dst:%p src:%p\n",dst,src); 
    /* Get destination device*/ 
    struct cudaPointerAttributes dst_att;
    memset(&dst_att,0,sizeof(dst_att));
    CUDA_ERROR(cudaPointerGetAttributes(&dst_att,dst),"cudaPointerGetAttribute");
    const int dst_dev = parse_cudaAtt(&dst_att,pt);
    
    /* Get source device */
    struct cudaPointerAttributes src_att;
    memset(&src_att,0,sizeof(src_att));
    CUDA_ERROR(cudaPointerGetAttributes(&src_att,src),"cudaPointerGetAttribute");
    const int src_dev = parse_cudaAtt(&src_att,pt);
    DEBUG_PRINT("dst_dev:%d src_dev:%d \n",dst_dev,src_dev);

     /* Something removed, program has to be recompiled (if err in del_path, things my be the same, but better safe thane sorry ) */
    pt->is_same = false;

    /* Remove the communication path */
    mcl_res_t err = del_path(dst,dst_dev,src,src_dev,pt);
    RETURN_ERROR(err);

    

    return MCL_SUCCESS;
ERROR_CUDA:
    return MCL_CUDA_ERROR;
ERROR_RETURN:
    return err;
}

/*
    For use by the comm compilers, uses the veiw var to go thourogh the stack. 
*/
/*
    set the stck_view var to first element
*/
void init_topo_node_stck_view(struct comm_topo_node *node) {
    assert(node->stck_view == NULL);
    node->stck_view = node->stck; //First element, or bottom stack if empty
    assert((node->stck_view->is_sentinal));
    assert(node->stck_view != NULL);
}

struct comm_path * next_topo_stck_view(struct comm_topo_node *node) {
    DEBUG_PRINT("stck_view:%p,is_bottom:%d\n",node->stck_view,(int)((uintptr_t)(node->stck_view) ^ (uintptr_t)(&node->bottom_sentinal))  );
    return (uintptr_t)(node->stck_view = node->stck_view->prv) ^ (uintptr_t)(&node->bottom_sentinal) ? (node->stck_view) : NULL;
}

void set_to_null_topo_node_stck_view(struct comm_topo_node *node) {
    node->stck_view = NULL; 
}

