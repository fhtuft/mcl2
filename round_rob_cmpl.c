/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "mcl_err.h"
#include "comm_cmpl.h"
#include "cmnd_engine.h"
#include "pttr.h"
#include "debug.h"



/*
    Simpel communication compiler, addes all commands from one node, before moving on to the next node

*/
mcl_res_t round_rob_cmpl(struct comm_pttr *comm_p) {


    /* Set the first command, we need min one (for halt) */
    comm_p->prg = comm_p->_mem_cmnd;
    struct cmnd *const first_cmnd = comm_p->prg;
    
    const int comm_topo_node_count = comm_p->comm_topo_node_count;
    const int comm_topo_x_dim = comm_p->comm_topo_x_dim;
    const int comm_topo_y_dim = comm_p->comm_topo_y_dim;
    /* One also for halt, not included her + other ops */
    int send_count = 0;  //Number of independen sends command, max of the different src_send_count

    struct comm_topo_node ** comm_topo = comm_p->comm_topo;


    /* init the stack view for all paths in the topology */ 
    struct cmnd *cur_cmnd = first_cmnd;
    for(int j = 0; j < comm_topo_y_dim; j++) {
        for(int i = 0; i < comm_topo_x_dim; i++) {
            const int index = j*comm_topo_x_dim + i;
            struct comm_topo_node *node = comm_topo[index];
                
            if(node) 
                init_topo_node_stck_view(node);
            
        }
    }


    int path_count;
    do {
    
    path_count = 0; // We count etch time

    for(int j = 0; j < comm_topo_y_dim; j++) {
        for(int i = 0; i < comm_topo_x_dim; i++) {
            const int index = j*comm_topo_x_dim + i;
            struct comm_topo_node *node = comm_topo[index];
                
            if(!node) 
                continue;
                
            struct comm_path *cp; 
            if( (cp  = next_topo_stck_view(node)) ) {
                path_count++;
                assert(!(cp->is_sentinal));
                /*  
                    Should be compiled to jmptbl, so not much loss from having 
                    it inside for loop, but idealy should have it outside, but more
                    cleanly this way.
                */
                switch(node->type) {
                    case COMM_PTTR_GPU_GPU:
                        DEBUG_PRINT("COMM_PTTR_GPU_GPU:j:%d:i:%d\n",j,i);
                        cur_cmnd->type = CMND_ENG_CMND_AP2P_S;
                        //DEBUG_PRINT("type:%s:i:%d:j:%d:cp->src:%p:cp->dst:%p:cp->size:%u\n",print_cmnd(cur_cmnd->type),i,j,cp->src,cp->dst,cp->size);
                        cur_cmnd->ap2p_s =(struct cmnd_eng_cmnd_ap2p_s ) {.dst_device = i, .src_device = j ,.sizeof_comms = cp->size  ,.src = cp->src ,.dst = cp->dst,.stream = comm_p->down_streams[j]  };
                         
                        DEBUG_PRINT("cur_cmnd->ap2p_s.dst_device:%d:src_device:%d,sizeof_comms:%u,src:%p:dst%p\n",cur_cmnd->ap2p_s.dst_device,cur_cmnd->ap2p_s.src_device,cur_cmnd->ap2p_s.sizeof_comms,cur_cmnd->ap2p_s.src,cur_cmnd->ap2p_s.dst);
                        cur_cmnd = cur_cmnd->nxt;
                        break;
                    case COMM_PTTR_CPU_GPU:
                        DEBUG_PRINT("COMM_PTTR_CPU_GPU:j:%d:i:%d\n",j,i);
                        cur_cmnd->type = CMND_ENG_CMND_AMEMCPY_HD_S ;
                        cur_cmnd->amemcpy_hd_s = (struct cmnd_eng_cmnd_amemcpy_hd_s  ) {.device = i ,.sizeof_comm = cp->size  ,.src = cp->src ,.dst = cp->dst,.stream = comm_p->up_streams[i] };
                         
                        DEBUG_PRINT("cur_cmnd->amemcpy_hd_s.device:%d,sizeof_comms:%u,src:%p:dst%p\n",cur_cmnd->amemcpy_hd_s.device,cur_cmnd->amemcpy_hd_s.sizeof_comm,cur_cmnd->amemcpy_hd_s.src,cur_cmnd->amemcpy_hd_s.dst);
                        
                        cur_cmnd = cur_cmnd->nxt;
                        break;
                    case COMM_PTTR_GPU_CPU:    
                        DEBUG_PRINT("COMM_PTTR_GPU_CPU:j:%d:i:%d\n",j,i);
                        cur_cmnd->type = CMND_ENG_CMND_AMEMCPY_DH_S ;
                        cur_cmnd->amemcpy_dh_s = (struct cmnd_eng_cmnd_amemcpy_dh_s  ) {.device = j ,.sizeof_comm = cp->size  ,.src = cp->src ,.dst = cp->dst,.stream = comm_p->down_streams[j] };
                         
                        DEBUG_PRINT("cur_cmnd->amemcpy_dh_s.device:%d,sizeof_comms:%u,src:%p:dst%p\n",cur_cmnd->amemcpy_dh_s.device,cur_cmnd->amemcpy_dh_s.sizeof_comm,cur_cmnd->amemcpy_dh_s.src,cur_cmnd->amemcpy_dh_s.dst);
                        
                        cur_cmnd = cur_cmnd->nxt;
                        break;
                    default:
                        assert(false); 
                }
             continue; // jump over cleanup code   
            }

            set_to_null_topo_node_stck_view(node);

            assert(node->stck_view == NULL);
            
        }
    }
   
    /*
       This checks that last iteration thourogh the topology nodes was empty or not  
    */
    }while(path_count); 
   
    /* Syncronice */
    const size_t gpu_count = comm_p->gpu_count;
        
    cur_cmnd->type = CMND_ENG_CMND_DEVSYNC;
    cur_cmnd->devsync.count = gpu_count;
    for(int i = 0;i < gpu_count; i++) {
        cur_cmnd->devsync.devices[i] = i;
    } 

    cur_cmnd = cur_cmnd->nxt;
   
 
    /* Terminate the command sequence with halt */
    cur_cmnd->type =CMND_ENG_CMND_HALT;   
    
    
    /*Gets send_count number of command ??*/    

    return MCL_SUCCESS;

}


#if TEST_ALL_CMPL


int main(int argc, char *argv[]) {
    
    assert(__builtin_types_compatible_p(all_cmpl,comm_cmpl_func_t)); 
    

}
#endif

