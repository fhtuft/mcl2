/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

#ifndef COMMON_H
#define COMMON_H
/*
    This file is included in all code files in the project, only internaly visible
*/
#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>

#include "mcl_err.h"
#include "debug.h"

#define RETURN_ERROR(ERR) if(ERR != MCL_SUCCESS) {goto ERROR_RETURN;} 


#define CUDA_CHECK(ERROR_CODE,FUNCTION) _cuda_error((ERROR_CODE),(FUNCTION),__LINE__,__FILE__)

#define CUDA_ERROR(ERROR_CODE,FUNCTION) if(_cuda_error((ERROR_CODE),(FUNCTION),__LINE__,__FILE__)) {goto ERROR_CUDA;}
static mcl_res_t  _cuda_error(cudaError_t error,const char *function,const int line,const char *filename) {

  if(error != cudaSuccess) {
#if DEBUG
    fprintf(stderr,"CUDA ERROR: %s ,function: %s line: %d  %s \n",
	    cudaGetErrorString(error),function,line,filename);
  
#else 
     fprintf(stderr,"MCL: ERROR IN CUDA %s\n",
	    cudaGetErrorString(error));
#endif
    return MCL_CUDA_ERROR;   
  }
    return MCL_SUCCESS;

}



#endif
