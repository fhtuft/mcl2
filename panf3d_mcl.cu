/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/


#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <stdlib.h> //exit

#include "cuda_util.h"
#include "panf3d_config.h"

extern "C" {
#include "mcl.h"
}


__global__ void noFluxFront(double * __restrict__ e_prev) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = 0*K_OFFSET+ j*J_OFFSET + i;
   #ifdef DEBUG 
    if((i < NX + 1) && (j < NY +1)) {
    #endif
        e_prev[index] = e_prev[index + 2*K_OFFSET];
    #ifdef DEBUF
    }
    #endif
}


__global__ void noFluxBack(double * __restrict__ e_prev) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = (NZ + 1)*K_OFFSET+ j*J_OFFSET + i;
#ifdef DEBUG  
    if((i < NX + 1) && (j < NY + 1)) {
#endif
        e_prev[index] = e_prev[index - 2*K_OFFSET]; 
#ifdef DEBUG
    }
#endif 
}

    

__global__ void noFluxNorth(double * __restrict__ e_prev) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET+ 0*J_OFFSET + i;
    #ifdef DEBUG
    if((i < NX + 1) && (k < NZ +1)) {
#endif
        e_prev[index] = e_prev[index + 2*J_OFFSET];
    #ifdef DEBUG
    }
    #endif
}

__global__ void noFluxSouth(double * __restrict__ e_prev) {

    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET + (NY +1)*J_OFFSET + i;
    #ifdef DEBUG
    if((i < NX + 1) && (k < NY + 1)) {
    #endif
        e_prev[index] = e_prev[index - 2*J_OFFSET];
    #ifdef DEBUG
    }
    #endif
}
//DEP
__global__ void noFluxWest(double * __restrict__ e_prev) {

    const int j = 1 + threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET+ j*J_OFFSET + (NX +1);
  #ifdef DEBUG
    if((j < NY + 1) && (k < NZ +1)) {
    #endif
        e_prev[index] = e_prev[index - 2];
    #ifdef DEBUG
    }
    #endif
}
//DEP
__global__ void noFluxEast(double * __restrict__ e_prev) {

    const int j = 1 + threadIdx.x + blockIdx.x*blockDim.x;
    const int k = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    
    const int index = k*K_OFFSET+ j*J_OFFSET  + 0;
   #ifdef DEBUG
    if((j < NY + 1) && (k < NZ +1)) {
     #endif
        e_prev[index] = e_prev[index + 2];
    #ifdef DEBUG
    }
    #endif
}


__global__ void cpy_to_gc(double * e_cur,double * gc,const int k ) {
   
    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;

    const int e_cur_index = k*K_OFFSET + j*J_OFFSET + i;

    const int gc_index =  j*J_OFFSET + i;    
 
    gc[gc_index] = e_cur[e_cur_index];


}

__global__ void cpy_from_gc(double  * e_cur,double * gc,const int k ) {
   
    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;

    const int e_cur_index = k*K_OFFSET + j*J_OFFSET + i;

    const int gc_index =  j*J_OFFSET + i;    
 
     e_cur[e_cur_index] = gc[gc_index];

}

__global__ void solvPanf3d( double const *__restrict__ const e_prv,double *__restrict__  e_cur, double * __restrict__ r_cur) {

    __shared__ double sharedMem[BLOCK_DIM_X__K+1][BLOCK_DIM_Y__K+1];
    
    const int i = threadIdx.x + blockIdx.x*blockDim.x;
    const int j = 1 + threadIdx.y + blockIdx.y*blockDim.y;
    const int k = 1 + blockIdx.z*BLOCK_DIM_Z_FOR;

    int index = k*K_OFFSET + j*J_OFFSET + i;
   
    register double e_left;
    register double e_right;

    #ifdef DEBGU
    if((i < NX +1)  && (j < NY +1) ) {
    #endif
        register double center =  e_prv[index];
        register double north = e_prv[index + K_OFFSET];
        register double south = e_prv[index - K_OFFSET];
         sharedMem[threadIdx.x+1][threadIdx.y+1] = center; 
        if(threadIdx.x == 0 || threadIdx.x == blockDim.x) {
            const register int shared_index = index + ((threadIdx.x == 0)? -1: -1)*K_OFFSET; //TODO ?? -1
            sharedMem[threadIdx.x][threadIdx.y] = e_prv[shared_index];
        } 
        if(threadIdx.y == 0 || threadIdx.y == blockDim.y) {
            const register int shared_index = index + ((threadIdx.y == 0)? -1: -1)*J_OFFSET;
            sharedMem[threadIdx.x][threadIdx.y] = e_prv[shared_index];
        }  

 
 if(i == 0)  {

            e_left = e_prv[index +1];



    } else {
        e_left = e_prv[index -1];

    }
    if(i == (NX -1)) {
        e_right = e_prv[index -1];
    } else {
        e_right = e_prv[index + 1];

    }    
 
        e_cur[index] = center + ALPHA*(e_left + e_right - 6.0*center + e_prv[index + J_OFFSET] + e_prv[index - J_OFFSET] + north + south);

        e_cur[index] += -DT*(KK*e_cur[index]*(e_cur[index]-A)*(e_cur[index] - 1.0) +e_cur[index]*r_cur[index]);
        r_cur[index] += DT*(EPSILON + M1*r_cur[index]/(e_cur[index] + M2))*(-r_cur[index] - KK*e_cur[index]*(e_cur[index] - B-1.0));         
    
    
    #pragma unroll 
    for(int __k = 1 ; __k< BLOCK_DIM_Z_FOR; __k++) {
        #ifdef DEBUG
        if((__k + k ) < (NZ +1)) {
     #endif
            index += K_OFFSET;
            south = center;
            center = north;
            north = e_prv[index +K_OFFSET];
    
    if(i == 0)  {
        
            e_left = e_prv[index +1];


    } else {
        e_left = e_prv[index -1];

    }
    if(i == (NX-1)) {
        //printf("blockId.x:%d Threadcount:%d threadIdx.x:%d  blockDim.x:%d\n",blockIdx.x,THREAD_COUNT_X__K,threadIdx.x,blockDim.x);
        e_right = e_prv[index-1];
    } else {
        e_right = e_prv[index + 1];

    }    


 
            e_cur[index] = center + ALPHA*(e_left + e_right - 6.0*center + e_prv[index + J_OFFSET] + e_prv[index - J_OFFSET] + north + south);

            e_cur[index] += -DT*(KK*e_cur[index]*(e_cur[index]-A)*(e_cur[index]-1.0) + e_cur[index]*r_cur[index]);
            r_cur[index] += DT*(EPSILON + M1*r_cur[index]/(e_cur[index] + M2))*(-r_cur[index]-KK*e_cur[index]*(e_cur[index] - B-1.0));     
       #ifdef DEBUG
         }
        #endif
    }
   #ifdef DEBUG

    }
    #endif
    
}

int main(int argc, char *argv[]) {

    if(argc != 2) {
        fprintf(stderr,"wrong number of args\n"); exit(0);
    }
       
           
    const int max_iters = atoi(argv[1]);

    const size_t sizeof_gpu_arrays = (NX)*(NY+2U)*(NZ* + 2U)*sizeof(double);
    const size_t sizeof_arrays = (NX)*(NY+2U)*(NZ*GPU_COUNT + 2U)*sizeof(double);
    double *e_cur,*e_prev,*r_cur;

    CUDA_ERROR(cudaHostAlloc((void**)&e_cur,sizeof_arrays,cudaHostAllocDefault),
    "cudaHostAlloc");
    CUDA_ERROR(cudaHostAlloc((void**)&e_prev,sizeof_arrays,cudaHostAllocDefault),
    "cudaHostAlloc");
    CUDA_ERROR(cudaHostAlloc((void**)&r_cur,sizeof_arrays,cudaHostAllocDefault),
    "cudaHostAlloc");

    for(unsigned int k = 1U; k <= NZ*GPU_COUNT; k++) {
        for(unsigned int j = 1U; j <= NY; j++)  {
            for(unsigned int i = 0U; i < NX; i++) {
                const unsigned int index = k*(NX)*(NY+2U) + j*(NX) + i;
                e_prev[index] = ( i > NX/2U && k > (NZ*GPU_COUNT)/2U ) ? 1.0 : 0.0;
	            r_cur[index] = ( j > NY/2U && k > (NZ*GPU_COUNT)/2U ) ? 1.0 : 0.0;  
            }
        }
    }

    double *e_cur__device[GPU_COUNT];
    double *e_prev__device[GPU_COUNT];
    double *r_cur__device[GPU_COUNT];
    double *be_in_buf[GPU_COUNT];
    double *be_out_buf[GPU_COUNT];

    const size_t sizeof_buf = (NX)*(NY+2U)*sizeof(double);


    for(int i = 0; i<GPU_COUNT; i++) {
        CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
        CUDA_ERROR(cudaMalloc((void**)&e_cur__device[i],sizeof_arrays),
        "cudaMalloc");
        CUDA_ERROR(cudaMalloc((void**)&e_prev__device[i],sizeof_arrays),
        "cudaMalloc");
        CUDA_ERROR(cudaMalloc((void**)&r_cur__device[i],sizeof_arrays),
        "cudaMalloc");
        CUDA_ERROR(cudaMalloc((void**)&be_in_buf[i],sizeof_buf),"cudaMalloc");
        CUDA_ERROR(cudaMalloc((void**)&be_out_buf[i],sizeof_buf),"cudaMalloc");
    }  


    for(int i = 0; i<GPU_COUNT; i++) {
        CUDA_ERROR(cudaMemcpy(e_cur__device[i],e_cur,sizeof_arrays,cudaMemcpyHostToDevice),"cudaMamcpy"); 
        CUDA_ERROR(cudaMemcpy(e_prev__device[i],e_prev,sizeof_arrays,cudaMemcpyHostToDevice),"cudaMemcpy"); 
        CUDA_ERROR(cudaMemcpy(r_cur__device[i],r_cur,sizeof_arrays,cudaMemcpyHostToDevice),"cudaMemcpy"); 
    }
    const dim3 threadBlocks__Kernel(THREAD_COUNT_X__K,THREAD_COUNT_Y__K,THREAD_COUNT_Z__K);
    const dim3 threadsPerBlock__Kernel(BLOCK_DIM_X__K,BLOCK_DIM_Y__K,BLOCK_DIM_Z__K);
    const dim3 threadBlocks__BC(THREAD_COUNT_X__BC,THREAD_COUNT_Y__BC,1);
    const dim3 threadsPerBlock__BC(BLOCK_DIM_X__BC,BLOCK_DIM_Y__BC,1);
 
    /* Stream */
    cudaStream_t compute_stream[GPU_COUNT];
    cudaStream_t comm_stream[GPU_COUNT];
    cudaStream_t north_stream[GPU_COUNT],south_stream[GPU_COUNT],west_stream[GPU_COUNT],east_stream[GPU_COUNT],front_stream[GPU_COUNT],back_stream[GPU_COUNT];
    for(int i = 0; i < GPU_COUNT; i++)  {
        CUDA_ERROR(cudaStreamCreate(&compute_stream[i]),"cudaStreamCreate");
        CUDA_ERROR(cudaStreamCreate(&comm_stream[i]),"cudaStreamCreate");
        CUDA_ERROR(cudaStreamCreate(&north_stream[i]),"cudaStreamCreate");
        CUDA_ERROR(cudaStreamCreate(&south_stream[i]),"cudaStreamCreate");
        CUDA_ERROR(cudaStreamCreate(&west_stream[i]),"cudaStreamCreate");
        CUDA_ERROR(cudaStreamCreate(&east_stream[i]),"cudaStreamCreate");
        CUDA_ERROR(cudaStreamCreate(&front_stream[i]),"cudaStreamCreate");
        CUDA_ERROR(cudaStreamCreate(&back_stream[i]),"cudaStreamCreate");
    }

    cudaEvent_t events[GPU_COUNT];
    for(int i = 0; i<GPU_COUNT; i++) {
        CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");    
        CUDA_ERROR(cudaEventCreateWithFlags(&events[i],cudaEventDisableTiming),"cudaEventCreateWithFlag"); 
    }

    static double time__omp = 0.0;
    time__omp -= omp_get_wtime();

    //CUDA_ERROR(cudaDeviceSetCacheConfig(cudaFuncCachePreferL1),"cudaDeviceSetCachConfig");

    /* MCL PART */
    mcl_init();

    mcl_pttr_t *comm_pt;
    mcl_res_t err = mcl_alloc_pttr(&comm_pt,0);
    err = mcl_add_path(be_in_buf[0],be_out_buf[1],sizeof_buf,comm_pt);
    err = mcl_add_path(be_in_buf[1],be_out_buf[0],sizeof_buf,comm_pt);


 
    for(unsigned int j = 0U; j < max_iters; j++) {
       
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice"); 
            noFluxFront<<<threadBlocks__BC,threadsPerBlock__BC,0,comm_stream[i]>>>(e_prev__device[i]);
            noFluxBack<<<threadBlocks__BC,threadsPerBlock__BC,0,comm_stream[i]>>>(e_prev__device[i]);
            noFluxNorth<<<threadBlocks__BC,threadsPerBlock__BC,0,comm_stream[i]>>>(e_prev__device[i]);
            noFluxSouth<<<threadBlocks__BC,threadsPerBlock__BC,0,comm_stream[i]>>>(e_prev__device[i]);
            //noFluxWest<<<threadBlocks__BC,threadsPerBlock__BC,0,west_stream>>>(e_prev__device);
            //noFluxEast<<<threadBlocks__BC,threadsPerBlock__BC,0,east_stream>>>(e_prev__device);
        }
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSynchronice");
        }
        
        /* Launch kernel to compute then Copy to buffer */
        for(int i = 0; i< GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            solvPanf3d<<<threadBlocks__Kernel,threadsPerBlock__Kernel,0,compute_stream[i]>>>(e_prev__device[i],e_cur__device[i],r_cur__device[i]);
            const int this_k = (i%2)*NZ;
           cpy_to_gc<<<threadBlocks__BC,threadsPerBlock__BC,0,compute_stream[i]>>>(e_cur__device[i],be_out_buf[i],this_k);
        }
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSynchronice");
        }
    
        /* Copy from out buffer to in buffer  on the cards*/
        err = mcl_sync_excomm(comm_pt); 

        
        /* Copy from buffer into memory */
        for(int i = 0; i < GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            const int this_k = (i%2)*NZ;
            cpy_to_gc<<<threadBlocks__BC,threadsPerBlock__BC,0,compute_stream[i]>>>(e_cur__device[i],be_in_buf[i],this_k);               
        }
        //swap
        for(int i = 0; i<GPU_COUNT; i++){
                register double *tmp__e = e_cur__device[i];
                e_cur__device[i] = e_prev__device[i];
                e_prev__device[i] = tmp__e;
        }
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSynchronice");
        }
        
      
    }
    time__omp += omp_get_wtime();
    
    const double gflops = (max_iters * ((double)NX * (double)NY * (double)(NZ*GPU_COUNT)) * 1e-9 * FLOPS)/time__omp;


    for(int i = 0; i<GPU_COUNT; i++) {
        CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
        CUDA_ERROR(cudaMemcpy(e_prev,e_prev__device[i],sizeof_gpu_arrays,cudaMemcpyDeviceToHost),"cudaMemcpy");

    }
    double l2_e_prev = 0.f;
#if 1 
    for(int k = 1; k < NZ +1; k++) {
        for(int j = 1; j < NY +1; j++) {
            for(int i = 0; i < NX; i++) {
                const int index = k*K_OFFSET + j*J_OFFSET + i;
                l2_e_prev += e_prev[index]*e_prev[index];
            }
        }
    }
    printf("l2_e_prev :%f \n",l2_e_prev);
#endif
    const double global_l2_norme = sqrt(l2_e_prev/(NX*NY*NZ));    


    fprintf(stdout,"time: %f GFLOPS: %f, l2:%f\n",time__omp,gflops,global_l2_norme);
   


    CUDA_ERROR(cudaDeviceReset(),"cudaDeviceReset");

    exit(0);
    
}

