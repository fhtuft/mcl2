/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "mcl_err.h"
#include "comm_cmpl.h"

/* Compiler declarations: */
mcl_res_t all_cmpl(struct comm_pttr *cp);
mcl_res_t round_rob_cmpl(struct comm_pttr *cp);
mcl_res_t left_right_cmpl(struct comm_pttr *cp);
mcl_res_t brdc_cmpl(struct comm_pttr *cp);
mcl_res_t pair_cmpl(struct comm_pttr *cp);



mcl_res_t cmpl_select(comm_cmpl_func_t *cmpl,const mcl_comm_cmpl_t ctype) {

    switch(ctype) {
        case MCL_COMM_CMPL_ALL:
            *cmpl = all_cmpl;
            break;
         case MCL_COMM_CMPL_ROUND:
            *cmpl = round_rob_cmpl;
            break;
        case MCL_COMM_CMPL_KNAPSACK:
            *cmpl = NULL;
            break;
        case MCL_COMM_CMPL_LEFTRIGHT:
            *cmpl = left_right_cmpl;
            break;
        case MCL_COMM_CMPL_BRODCAST:    
            *cmpl = brdc_cmpl;
            break;
        case MCL_COMM_CMPL_PAIR:
             *cmpl = pair_cmpl;
            break;
        default:
            assert(0); //Programming error
            return MCL_ERROR;
    }

    return MCL_SUCCESS;
}





/*TODO: Link in the real deal! */
#if 0
mcl_res_t all_cmpl(struct comm_pttr *cp) {
    return MCL_SUCCESS; 
}
#endif
