
/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/


/**
* @file mcl_test.c
* @brief test functions in mcl.c 
* @author Finn-Haakon Tuft
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "unit_test.h"
#include "mcl_err.h"

#include "mcl.h"



#define TEST_ALLOC_FREE      0
#define TEST_ADD_RMV         0
#define TEST_ADD_RMV_SEND    1
#define TEST_SEND_PTTR       0 

int main(void) {
    START_TEST;
    mcl_res_t err;
    const size_t sizeof_test_array = 1000000;
    


    err = mcl_init();
    ASSERT_TEST("mcl_init ret value",err>= MCL_SUCCESS);

#if TEST_ALLOC_FREE   
    {
        PRINT_TEST("Testing pttr alloc and free");
        mcl_pttr_t *pt1,*pt2;
        err  = mcl_alloc_pttr(&pt1,0);
        ASSERT_TEST("Alloc pattr ret val",err >= MCL_SUCCESS);
        err  = mcl_alloc_pttr(&pt2,0);
        ASSERT_TEST("Alloc pattr ret val",err >= MCL_SUCCESS);
        err = mcl_free_pttr(pt1);
        ASSERT_TEST("Free pttr",err>= MCL_SUCCESS);
        err = mcl_free_pttr(pt2);
        ASSERT_TEST("Free pttr",err>= MCL_SUCCESS);
    }
#endif
    
#if TEST_ADD_RMV
    {
        PRINT_TEST("Testing adding and removing pttr");
        void *host_array1,*host_array2;
        mcl_pttr_t *pt;

        err  = mcl_alloc_pttr(&pt,0);
        ASSERT_TEST("Alloc pattr ret val",err >= MCL_SUCCESS);

        CUDA_CHECK(cudaMallocHost(&host_array1,sizeof_test_array),"cudaMallocHost");
        memset(host_array1,'A',sizeof_test_array);
        CUDA_CHECK(cudaMallocHost(&host_array2,sizeof_test_array),"cudaMallocHost");
        memset(host_array2,'0',sizeof_test_array);
        
        void *device0_array1,*device0_array2,*device0_array3;
        void *device1_array1,*device1_array2,*device1_array3;
        CUDA_CHECK(cudaSetDevice(0),"cudeSetDevice");
        CUDA_CHECK(cudaMalloc(&device0_array1,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device0_array2,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device0_array3,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaSetDevice(1),"cudaSetDevice");//For lizi
        CUDA_CHECK(cudaMalloc(&device1_array1,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device1_array2,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device1_array3,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaSetDevice(0),"cudeSetDevice");

        err = mcl_add_path(device0_array1,host_array1,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path host to device",err >= MCL_SUCCESS);
        err = mcl_add_path(host_array2,device1_array1,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);
        err = mcl_add_path(device0_array2,device1_array2,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);


        
        err = mcl_rmv_path(device0_array1,host_array1,sizeof_test_array,pt);
        ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);
        err = mcl_rmv_path(host_array2,device1_array1,sizeof_test_array,pt); 
        ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);
        err = mcl_rmv_path(device0_array2,device1_array2,sizeof_test_array,pt);
        ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);

        
        err = mcl_add_path(device0_array1,host_array1,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path host to device",err >= MCL_SUCCESS);
        err = mcl_add_path(host_array2,device1_array1,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);
        err = mcl_add_path(device0_array2,device1_array2,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);
        err = mcl_add_path(device0_array3,device1_array3,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);

        err = mcl_rmv_path(device0_array1,host_array1,sizeof_test_array,pt);
        ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);
        err = mcl_rmv_path(host_array2,device1_array1,sizeof_test_array,pt); 
        ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);
        err = mcl_rmv_path(device0_array2,device1_array2,sizeof_test_array,pt);
        ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);
        err = mcl_rmv_path(device0_array3,device1_array3,sizeof_test_array,pt);
        ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);

        err = mcl_free_pttr(pt);
        ASSERT_TEST("Free pttr ret val",err >= MCL_SUCCESS);
    }
#endif 
    
#if TEST_SEND_PTTR
    {
        PRINT_TEST("TESTING SENDING PATTERN");
        void *host_array1,*host_array2,*host_array_test_A;
        mcl_pttr_t *pt;

        err  = mcl_alloc_pttr(&pt,0);
        ASSERT_TEST("Alloc pattr ret val",err >= MCL_SUCCESS);

        CUDA_CHECK(cudaMallocHost(&host_array1,sizeof_test_array),"cudaMallocHost");
        memset(host_array1,'A',sizeof_test_array);
        CUDA_CHECK(cudaMallocHost(&host_array2,sizeof_test_array),"cudaMallocHost");
        memset(host_array2,'0',sizeof_test_array);
        CUDA_CHECK(cudaMallocHost(&host_array_test_A,sizeof_test_array),"cudaMallocHost");
        memset(host_array_test_A,'0',sizeof_test_array);

        
        void *device0_array1,*device0_array2,*device0_array3;
        void *device1_array1,*device1_array2,*device1_array3;
        CUDA_CHECK(cudaSetDevice(0),"cudeSetDevice");
        CUDA_CHECK(cudaMalloc(&device0_array1,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device0_array2,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device0_array3,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaSetDevice(1),"cudaSetDevice");//For lizi
        CUDA_CHECK(cudaMalloc(&device1_array1,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device1_array2,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device1_array3,sizeof_test_array),"cudaMalloc");
        /* Memset device array for device to host test */
        CUDA_CHECK(cudaMemset(device1_array1,'B',sizeof_test_array),"cudaMemset");  
        CUDA_CHECK(cudaSetDevice(0),"cudeSetDevice");
        

        err = mcl_add_path(device0_array1,host_array1,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path host to device",err >= MCL_SUCCESS);
        err = mcl_add_path(host_array2,device1_array1,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);
        err = mcl_add_path(device0_array2,device1_array2,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);
        err = mcl_add_path(device0_array3,device1_array3,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path device to devie",err >= MCL_SUCCESS);

        /* Sending data */
        err = mcl_sync_excomm(pt);
        ASSERT_TEST("mcl_sync_excomm",err >= MCL_SUCCESS);
     
        /* Test that data whas sent */
        CUDA_CHECK(cudaMemcpy(host_array_test_A,device0_array1,sizeof_test_array,cudaMemcpyDeviceToHost),"cudaMemCpy");
        ASSERT_TEST("first element of HtoD == A",((char*)host_array_test_A)[0] == 'A');
        ASSERT_TEST("last element of HtoD == A",((char*)host_array_test_A)[sizeof_test_array-1] == 'A');
        ASSERT_TEST("first element of DtoH == B",((char*)host_array2)[0] == 'A');
        ASSERT_TEST("last element of DtoH == B",((char*)host_array2)[sizeof_test_array-1] == 'A');
        
     
        /* Free the pattern */
        err = mcl_free_pttr(pt);
        ASSERT_TEST("Free pttr ret val",err >= MCL_SUCCESS);

    }
#endif
 

#if TEST_ADD_RMV_SEND
    {
        PRINT_TEST("Testing adding and removing pttr");
        void *host_array1,*host_array2;
        mcl_pttr_t *pt;

        err  = mcl_alloc_pttr(&pt,0);
        ASSERT_TEST("Alloc pattr ret val",err >= MCL_SUCCESS);

        CUDA_CHECK(cudaMallocHost(&host_array1,sizeof_test_array),"cudaMallocHost");
        memset(host_array1,'A',sizeof_test_array);
        CUDA_CHECK(cudaMallocHost(&host_array2,sizeof_test_array),"cudaMallocHost");
        memset(host_array2,'0',sizeof_test_array);
        
        void *device0_array1,*device0_array2,*device0_array3;
        void *device1_array1,*device1_array2,*device1_array3;
        CUDA_CHECK(cudaSetDevice(0),"cudeSetDevice");
        CUDA_CHECK(cudaMalloc(&device0_array1,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device0_array2,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device0_array3,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaSetDevice(1),"cudaSetDevice");//For lizi
        CUDA_CHECK(cudaMalloc(&device1_array1,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device1_array2,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaMalloc(&device1_array3,sizeof_test_array),"cudaMalloc");
        CUDA_CHECK(cudaSetDevice(0),"cudeSetDevice");

      //  err = mcl_add_path(device0_array1,host_array1,sizeof_test_array,pt);
      //  ASSERT_TEST("mcl_add_path host to device",err >= MCL_SUCCESS);
      //  err = mcl_add_path(host_array2,device1_array1,sizeof_test_array,pt);
      //  ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);
        err = mcl_add_path(device0_array2,device1_array2,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);


        
        /* Sending data */
     //   err = mcl_sync_excomm(pt);
     //   ASSERT_TEST("mcl_sync_excomm",err >= MCL_SUCCESS);
        

        
      //  err = mcl_rmv_path(device0_array1,host_array1,sizeof_test_array,pt);
      //  ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);
      //  err = mcl_rmv_path(host_array2,device1_array1,sizeof_test_array,pt); 
      //  ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);
        err = mcl_rmv_path(device0_array2,device1_array2,sizeof_test_array,pt);
        ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);

        
     //   err = mcl_add_path(device0_array1,host_array1,sizeof_test_array,pt);
     //   ASSERT_TEST("mcl_add_path host to device",err >= MCL_SUCCESS);
     //   err = mcl_add_path(host_array2,device1_array1,sizeof_test_array,pt);
     //   ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);
     //   err = mcl_add_path(device0_array2,device1_array2,sizeof_test_array,pt);
     //   ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);
        err = mcl_add_path(device0_array3,device1_array3,sizeof_test_array,pt);
        ASSERT_TEST("mcl_add_path device to host",err >= MCL_SUCCESS);


        /* Sending data */
        err = mcl_sync_excomm(pt);
        ASSERT_TEST("mcl_sync_excomm",err >= MCL_SUCCESS);



    //    err = mcl_rmv_path(device0_array1,host_array1,sizeof_test_array,pt);
    //    ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);
    //    err = mcl_rmv_path(host_array2,device1_array1,sizeof_test_array,pt); 
    //    ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);
     //   err = mcl_rmv_path(device0_array2,device1_array2,sizeof_test_array,pt);
     //   ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);
        err = mcl_rmv_path(device0_array3,device1_array3,sizeof_test_array,pt);
        ASSERT_TEST("mcl_rmv_path",err >= MCL_SUCCESS);

     


        err = mcl_free_pttr(pt);
        ASSERT_TEST("Free pttr ret val",err >= MCL_SUCCESS);
    }
#endif 



   
    PRINT_TEST("TEST FINALIZE");
    
    err = mcl_finalize();
    ASSERT_TEST("mcl_finalize return value",err >= MCL_SUCCESS); 

 


    END_TEST;
    return 0;

}
