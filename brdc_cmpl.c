/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "mcl_err.h"
#include "comm_cmpl.h"
#include "cmnd_engine.h"
#include "pttr.h"
#include "debug.h"



/*
    Broadcast communication generator more or less for  the simula HPC machine Al-Khwarizmi  
    This contains hardcoded parts as this system do not support finding topology level..


    A broadcast pattern
    
    ooooo
    xxxxx
    ooooo
    ooooo
    ooooo

    */

mcl_res_t brdc_cmpl(struct comm_pttr *comm_p) {

    /* Lizhi*/
    assert(0);   

    return MCL_SUCCESS;

}


#if TEST_ALL_CMPL


int main(int argc, char *argv[]) {
    
    assert(__builtin_types_compatible_p(all_cmpl,comm_cmpl_func_t)); 
    

}
#endif

