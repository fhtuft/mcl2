/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

/* SIMPEL UNIT TEST 
USE:
START_TEST;
ASSERT_TEST(msg,test); msg: const char[], test: bool
END_TEST; Summarize the test, not necessary
*/
#ifndef UNIT_TEST_H
#define UNIT_TEST_H

#include <stdio.h> //fprintf,
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

#define INIT_TEST  
#define START_TEST static int TEST_COUNT =0;static int TEST_PASSED =0;fprintf(stderr,"UNIT TEST OF FILE:%s DATA:%s \n",__FILE__,__DATE__)
#define ASSERT_TEST(msg,test) do { if(!(test)) {fprintf(stderr,"UNIT TEST"RED" [FAILED]"RESET);} else{fprintf(stderr,"UNIT TEST"GRN" [PASSED]"RESET);TEST_PASSED++;};fprintf(stderr," UNIT TEST ID:%d LINE:%d MSG:%s \n",TEST_COUNT,__LINE__,msg); TEST_COUNT++;}while(0)
#define END_TEST do {if(TEST_COUNT == TEST_PASSED) {fprintf(stderr,"UNIT TEST"GRN" [PASSED]"RESET);} else{fprintf(stderr,"UNIT TEST"RED" [FAILED]"RESET);};fprintf(stderr,"UNIT TEST OF %s PASSED: %d of %d tests\n",__FILE__,TEST_PASSED,TEST_COUNT);}while(0)
#define PRINT_TEST(msg) fprintf(stderr,"UNIT TEST MSG:%s \n",msg)

#endif
