#ifndef CMND_ENGINE_H
#define CMND_ENGINE_H

#include <cuda_runtime.h>
#include <stdbool.h>

#include "config.h"


struct comm_pttr; //Forward decl

typedef enum {
    CMND_ENG_CMND_AP2P = 0,
    CMND_ENG_CMND_AP2P_REVENT  =1 ,
    CMND_ENG_CMND_AP2P_S ,
    CMND_ENG_CMND_AP2P_REVENT_S ,
    CMND_ENG_CMND_REVENT ,
    CMND_ENG_CMND_REVNET_S ,
    CMND_ENG_CMND_WEVENT ,
    CMND_ENG_CMND_MEMCPY_HD,
    CMND_ENG_CMND_MEMCPY_DH ,
    CMND_ENG_CMND_AMEMCPY_HD ,
    CMND_ENG_CMND_AMEMCPY_DH ,
    CMND_ENG_CMND_AMEMCPY_HD_S,
    CMND_ENG_CMND_AMEMCPY_DH_S,
    CMND_ENG_CMND_DEVSYNC ,
    CMND_ENG_CMND_HALT ,
    CMND_ENG_CMND_CAP, /*Never used in the program, assert on that*/ 
}cmnd_eng_cmnd_t;


struct cmnd_eng_cmnd_ap2p_revent_s {//Single
    int dst_device;
    int src_device;
    size_t sizeof_comms;
    void *src;
    void *dst;
    cudaStream_t stream;
    cudaEvent_t event;
};
struct cmnd_eng_cmnd_ap2p_s { //Single
    int dst_device;
    int src_device;
    size_t sizeof_comms;
    void *src;
    void *dst;
    cudaStream_t stream;
};
struct cmnd_eng_cmnd_revent_s { //Single
    int device;
    cudaStream_t stream;
    cudaEvent_t event;
};


struct cmnd_eng_cmnd_ap2p_revent {
    size_t count;
    int dst_device[CNF_MAX_DEVICES];
    int src_device[CNF_MAX_DEVICES];   
    size_t sizeof_comms[CNF_MAX_DEVICES];
    void *src[CNF_MAX_DEVICES];
    void *dst[CNF_MAX_DEVICES];
    cudaStream_t streams[CNF_MAX_DEVICES];
    cudaEvent_t events[CNF_MAX_DEVICES];
};

struct cmnd_eng_cmnd_ap2p {
    size_t count;
    int dst_device[CNF_MAX_DEVICES];
    int src_device[CNF_MAX_DEVICES];   
    size_t sizeof_comms[CNF_MAX_DEVICES];
    void *src[CNF_MAX_DEVICES];
    void *dst[CNF_MAX_DEVICES];
    cudaStream_t streams[CNF_MAX_DEVICES];
};

struct cmnd_eng_cmnd_revent {
    size_t count;
    int device[CNF_MAX_DEVICES];
    cudaStream_t streams[CNF_MAX_DEVICES];
    cudaEvent_t events[CNF_MAX_DEVICES];
};

struct cmnd_eng_cmnd_wevent { //TODO make event a anon struct in those that need??
    size_t count;
    bool has_occurred[CNF_MAX_DEVICES];
    int device[CNF_MAX_DEVICES];
    cudaStream_t streams[CNF_MAX_DEVICES];
    cudaEvent_t events[CNF_MAX_DEVICES];
};

struct cmnd_eng_cmnd_memcpy_hd {
    size_t count;
    size_t sizeof_comms[CNF_MAX_DEVICES];
    int device[CNF_MAX_DEVICES];
    void* src[CNF_MAX_DEVICES];
    void* dst[CNF_MAX_DEVICES]; 
};
struct cmnd_eng_cmnd_memcpy_dh {
    size_t count;
    int device[CNF_MAX_DEVICES];
    size_t sizeof_comms[CNF_MAX_DEVICES];
    void* src[CNF_MAX_DEVICES];
    void* dst[CNF_MAX_DEVICES]; 
};
struct cmnd_eng_cmnd_amemcpy_dh {
    size_t count;
    size_t sizeof_comms[CNF_MAX_DEVICES];
    int device[CNF_MAX_DEVICES];
    void* src[CNF_MAX_DEVICES];
    void* dst[CNF_MAX_DEVICES]; 
    cudaStream_t streams[CNF_MAX_DEVICES];
};
struct cmnd_eng_cmnd_amemcpy_hd {
    size_t count;
    int device[CNF_MAX_DEVICES];
    size_t sizeof_comms[CNF_MAX_DEVICES];
    void* src[CNF_MAX_DEVICES];
    void* dst[CNF_MAX_DEVICES]; 
    cudaStream_t streams[CNF_MAX_DEVICES];
};
struct cmnd_eng_cmnd_amemcpy_hd_s {
    int device;//To copy to 
    size_t sizeof_comm;
    void* src;
    void* dst; 
    cudaStream_t stream;
};
struct cmnd_eng_cmnd_amemcpy_dh_s {
    int device; //To copy from
    size_t sizeof_comm;
    void* src;
    void* dst; 
    cudaStream_t stream;
};



struct cmnd_eng_cmnd_devsync {
    size_t count;
    int devices[CNF_MAX_DEVICES];
};

struct cmnd_eng_cmnd_halt {
  int nothing_her;  
};



struct cmnd {
    bool alloced;
    cmnd_eng_cmnd_t type;
     union {
        struct cmnd_eng_cmnd_ap2p ap2p;
        struct cmnd_eng_cmnd_ap2p_s ap2p_s;
        struct cmnd_eng_cmnd_ap2p_revent ap2p_revent;
        struct cmnd_eng_cmnd_ap2p_revent_s ap2p_revent_s;
        struct cmnd_eng_cmnd_revent revent;
        struct cmnd_eng_cmnd_revent_s revent_s;
        struct cmnd_eng_cmnd_wevent wevent;
        struct cmnd_eng_cmnd_memcpy_hd memcpy_hd;
        struct cmnd_eng_cmnd_memcpy_dh memcpy_dh;
        struct cmnd_eng_cmnd_amemcpy_hd amemcpy_hd;
        struct cmnd_eng_cmnd_amemcpy_dh amemcpy_dh;
        struct cmnd_eng_cmnd_amemcpy_hd_s amemcpy_hd_s;
        struct cmnd_eng_cmnd_amemcpy_dh_s amemcpy_dh_s; 
        struct cmnd_eng_cmnd_devsync devsync;
        struct cmnd_eng_cmnd_halt halt;        
    };
    struct cmnd *nxt;
};


#include "pttr.h"

//TODO: forward decl of struct comm_pttr ??
mcl_res_t run_cmnd_engine(struct comm_pttr *pt); 
void debug_print_cmnds(struct comm_pttr *pt);
const char * print_cmnd(const cmnd_eng_cmnd_t type);

#endif
