/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/


/* 
 Util for cuda stuff
*/
#ifndef CUDA_UTIL_H
#define CUDA_UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>



/*- CUDA error handling -*/
#define CUDA_ERROR(ERROR_CODE,FUNCTION) __cuda_error((ERROR_CODE),(FUNCTION),__LINE__,__FILE__)
static inline void __cuda_error(cudaError_t error,const char *function,const int line,const char *filename) {

  if(error != cudaSuccess) {
    fprintf(stderr,"CUDA ERROR: %s ,function: %s line: %d  %s \n",
	    cudaGetErrorString(error),function,line,filename);
    exit(EXIT_FAILURE);
  }
    
}


#endif
