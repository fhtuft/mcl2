/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <stdlib.h> //exit
#include <unistd.h>
#include <cuda_runtime.h>

//#include "common.h"
extern "C" {

#include "mcl.h"
}

#define CUDA_ERROR(ERROR_CODE,FUNCTION) _cuda_error((ERROR_CODE),(FUNCTION),__LINE__,__FILE__)
static mcl_res_t  _cuda_error(cudaError_t error,const char *function,const int line,const char *filename) {

  if(error != cudaSuccess) {
#if DEBUG
    fprintf(stderr,"CUDA ERROR: %s ,function: %s line: %d  %s \n",
	    cudaGetErrorString(error),function,line,filename);
  
#else 
     fprintf(stderr,"MCL: ERROR IN CUDA %s\n",
	    cudaGetErrorString(error));
#endif
    return MCL_CUDA_ERROR;   
  }
    return MCL_SUCCESS;

}




#define GPU_COUNT    2
#define MILLION       1000000
#define TEST1_COUNT  200*MILLION
#define TEST1_SIZEOF (TEST1_COUNT)*sizeof(double)
#define TEST1  0
#define TEST1_MCL 0
#define TEST2  0
#define TEST2_MCL 0
#define TEST3  0
#define TEST3_MCL  0
#define STREAM_TEST4 0
#define STREAM_TEST4_MCL 0
#define STREAM_TEST5 0
#define STREAM_TEST5_MCL 0
#define STREAM_TEST6 0
#define STREAM_TEST6_MCL 0
#define STREAM_TEST7 0
#define STREAM_TEST7_MCL 0
#define STREAM_TEST8 0
#define STREAM_TEST8_MCL 0
#define STREAM_TEST9 1
#define STREAM_TEST9_MCL 1

int main(int argc, char *argv[]) {

    if(argc != 1) {
        fprintf(stderr,"wrong number of args! \n"); 
        exit(EXIT_FAILURE);
    } 

/*
    for(int i = 0; i < GPU_COUNT; i++) {
        CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
	for(int j = (i+1)%GPU_COUNT; j != i; j= (j+1)%GPU_COUNT) {
        	CUDA_ERROR(cudaDeviceEnablePeerAccess(j,0),"cudaDeviceEnablePeerAcces");
    	}
    }

  */  
    double *test1[GPU_COUNT];
    double *test1_host[GPU_COUNT]; 
    //double *test2_src[GPU_COUNT];
    //double *test2_dst[GPU_COUNT];
    for(int i  = 0; i<GPU_COUNT; i++) {
        CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
        CUDA_ERROR(cudaMalloc((void**)&test1[i],TEST1_SIZEOF),"cudaMalloc");
        CUDA_ERROR(cudaMallocHost((void**)&test1_host[i],TEST1_SIZEOF),"cudaHostAlloc");
        //CUDA_ERROR(cudaMalloc((void**)&test2_src[i],TEST1_SIZEOF),"cudaMalloc");
        //CUDA_ERROR(cudaMalloc((void**)&test2_dst[i],TEST1_SIZEOF),"cudaMalloc");
    } 

    cudaStream_t streams1[GPU_COUNT]; 
    cudaStream_t streams2[GPU_COUNT]; 
    cudaStream_t streams3[GPU_COUNT];
    cudaStream_t streams4[GPU_COUNT];  
   
    for(int i = 0; i< GPU_COUNT; i++) {
        CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
        CUDA_ERROR(cudaStreamCreate(&streams1[i]),"cudaStreamCreate()");
	CUDA_ERROR(cudaStreamCreate(&streams2[i]),"cudaStreamCreate()");
	CUDA_ERROR(cudaStreamCreate(&streams3[i]),"cudaStreamCreate()");
	CUDA_ERROR(cudaStreamCreate(&streams4[i]),"cudaStreamCreate()");
    }

    cudaEvent_t events[GPU_COUNT];        

    for(int i = 0; i< GPU_COUNT; i++) {
    	CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
    	CUDA_ERROR(cudaEventCreateWithFlags(&events[i],cudaEventDisableTiming),"cudaEvent");
    }

     int test_size = 500000000; 



    static double time__omp = 0.0;
    time__omp -= omp_get_wtime();

    mcl_res_t err = mcl_init();
   
do {
      
         //for(int test_size = 1000; test_size< TEST1_SIZEOF; test_size*=2 ) {
    	int trans_count  =0; // Number of transfers
	int dst = 0;
	int src = 0;



/* START OF THIS TEST   */
#if TEST1

	printf("Test of 0-1  \n"); 
	trans_count = 1;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
	src = 0; dst = 1;
        CUDA_ERROR(cudaSetDevice(src),"cudaSetDevice");
        CUDA_ERROR(cudaMemcpyPeerAsync(test1[dst],dst,test1[src],src,test_size,streams1[src]),"cudaMemcpyPerrAsync");
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSync");
        }
        time__omp += omp_get_wtime();
        fprintf(stdout,"time: %f,test size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(test_size)*(1e-6),(trans_count*(test_size)/time__omp)*1e-9);
/*  END OF THIS THIST */
#endif
#if TEST1_MCL
{    
    mcl_pttr_t *pt1,*pt2;
    err  = mcl_alloc_pttr(&pt1,0);
    err = mcl_add_path(test1[1],test1[0],test_size,pt1);


	printf("Test mcl of 0-1  \n"); 
	trans_count = 1;
    time__omp = 0.0;
    time__omp -= omp_get_wtime();        
    err = mcl_sync_excomm(pt1);
	                
    time__omp += omp_get_wtime();
    fprintf(stdout,"time: %f,test size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(test_size)*(1e-6),(trans_count*(test_size)/time__omp)*1e-9);

}
/*  END OF THIS THIST */
#endif

#if TEST2

	printf("Test of host to 0   \n"); 
	trans_count = 1;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
	src = 0; dst = 1;
        CUDA_ERROR(cudaSetDevice(src),"cudaSetDevice");
        CUDA_ERROR(cudaMemcpyAsync(test1[src],test1_host[src],test_size,cudaMemcpyHostToDevice),"cudaMemcpyPerrAsync");
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSync");
        }
        time__omp += omp_get_wtime();
        fprintf(stdout,"time: %f,test size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(test_size)*(1e-6),(trans_count*(test_size)/time__omp)*1e-9);
/*  END OF THIS THIST */
#endif


#if TEST2_MCL
{    
    mcl_pttr_t *pt1,*pt2;
    err  = mcl_alloc_pttr(&pt1,0);
    err = mcl_add_path(test1[0],test1_host[0],test_size,pt1);


	printf("Test mcl host to 0  \n"); 
	trans_count = 1;
    time__omp = 0.0;
    time__omp -= omp_get_wtime();        
    err = mcl_sync_excomm(pt1);
	                
    time__omp += omp_get_wtime();
    fprintf(stdout,"MCL:time: %f,test size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(test_size)*(1e-6),(trans_count*(test_size)/time__omp)*1e-9);

}
/*  END OF THIS THIST */
#endif
#if TEST3

	printf("Test of  0 to host   \n"); 
	trans_count = 1;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
	src = 0; dst = 1;
        CUDA_ERROR(cudaSetDevice(src),"cudaSetDevice");
        CUDA_ERROR(cudaMemcpyAsync(test1_host[src],test1[src],test_size,cudaMemcpyDeviceToHost),"cudaMemcpyPerrAsync");
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSync");
        }
        time__omp += omp_get_wtime();
        fprintf(stdout,"time: %f,test size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(test_size)*(1e-6),(trans_count*(test_size)/time__omp)*1e-9);
/*  END OF THIS THIST */
#endif


#if TEST3_MCL
{    
    mcl_pttr_t *pt1,*pt2;
    err  = mcl_alloc_pttr(&pt1,0);
    err = mcl_add_path(test1_host[0],test1[0],test_size,pt1);


	printf("Test mcl 0 to host  \n"); 
	trans_count = 1;
    time__omp = 0.0;
    time__omp -= omp_get_wtime();        
    err = mcl_sync_excomm(pt1);
	                
    time__omp += omp_get_wtime();
    fprintf(stdout,"MCL:time: %f,test size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(test_size)*(1e-6),(trans_count*(test_size)/time__omp)*1e-9);

}
/*  END OF THIS THIST */
#endif

#if STREAM_TEST4 /*DEVICE TO HOST*/

    for(size_t stream_size  = 250000; stream_size <= 1024000000; stream_size *=2) { 

	    printf("TEST STREAM DEVICE TO HOST  \n"); 
	    trans_count = 1;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
	    src = 0; dst = 1;
        CUDA_ERROR(cudaSetDevice(src),"cudaSetDevice");
        CUDA_ERROR(cudaMemcpyAsync(test1_host[src],test1[src],stream_size,cudaMemcpyDeviceToHost),"cudaMemcpyPerrAsync");
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSync");
        }
        time__omp += omp_get_wtime();
        fprintf(stdout,"time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);

    }
/*  END OF THIS THIST */
#endif


#if STREAM_TEST4_MCL /* DEVICE TO HOST*/
{    
    mcl_pttr_t *pt1,*pt2;
    err  = mcl_alloc_pttr(&pt1,0);
    
    for(size_t stream_size  = 250000; stream_size<= 1024000000; stream_size*=2 ) { 

        err = mcl_add_path(test1_host[0],test1[0],stream_size,pt1);


	    printf("TEST STREAM DEVICE TO HOST MCL  \n"); 
    	trans_count = 1;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
        err = mcl_sync_excomm(pt1);
	                
        time__omp += omp_get_wtime();
        fprintf(stdout,"MCL:time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);
    
        err = mcl_rmv_path(test1_host[0],test1[0],stream_size,pt1);
    }

}
#endif
#if STREAM_TEST5 /* Host to Device*/

    for(size_t stream_size  = 250000; stream_size <= 1024000000; stream_size *=2) { 

	    printf("TEST STREAM HOST TO DEVICE   \n"); 
	    trans_count = 1;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
	    src = 0; dst = 1;
        CUDA_ERROR(cudaSetDevice(src),"cudaSetDevice");
        CUDA_ERROR(cudaMemcpyAsync(test1[src],test1_host[src],stream_size,cudaMemcpyHostToDevice),"cudaMemcpyPerrAsync");
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSync");
        }
        time__omp += omp_get_wtime();
        fprintf(stdout,"time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);

    }
/*  END OF THIS THIST */
#endif


#if STREAM_TEST5_MCL /* Host to Device */
{   
    CUDA_ERROR(cudaSetDevice(0),"cudaSetDevice"); 
    mcl_pttr_t *pt1,*pt2;
    
    for(size_t stream_size  = 250000; stream_size<= 1024000000; stream_size*=2 ) { 
    err  = mcl_alloc_pttr(&pt1,0);
        
        err = mcl_add_path(test1[0],test1_host[0],stream_size,pt1);


	    printf("TEST STREAM HOST TO DEVICE  \n"); 
    	trans_count = 1;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
        err = mcl_sync_excomm(pt1);
	                
        time__omp += omp_get_wtime();
        fprintf(stdout,"MCL:time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);
    
        err = mcl_rmv_path(test1_host[0],test1[0],stream_size,pt1);
    }

}
#endif

#if STREAM_TEST6 /* Host to Host */

    for(size_t stream_size  = 250000; stream_size <= 1024000000; stream_size *=2) { 

	    printf("TEST OF STREAM HOST TO HOST HARDCODED \n"); 
	    trans_count = 1;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
	    src = 0; dst = 1;
        CUDA_ERROR(cudaSetDevice(src),"cudaSetDevice");
        CUDA_ERROR(cudaMemcpyAsync(test1[dst],test1[src],stream_size,cudaMemcpyDeviceToDevice),"cudaMemcpyPerrAsync");
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSync");
        }
        time__omp += omp_get_wtime();
        fprintf(stdout,"time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);

    }
/*  END OF THIS THIST */
#endif


#if STREAM_TEST6_MCL /* Host To Host */
{    
    mcl_pttr_t *pt1,*pt2;
    err  = mcl_alloc_pttr(&pt1,0);
    
    for(size_t stream_size  = 250000; stream_size<= 1024000000; stream_size*=2 ) { 
        err  = mcl_alloc_pttr(&pt1,0);
        err = mcl_add_path(test1[1],test1[0],stream_size,pt1);


	    printf("TEST STREAM HOST TO HOST MCL \n"); 
    	trans_count = 1;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
        err = mcl_sync_excomm(pt1);
	                
        time__omp += omp_get_wtime();
        fprintf(stdout,"MCL:time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);
    
        err = mcl_rmv_path(test1_host[0],test1[0],stream_size,pt1);
    }

}
#endif

#if STREAM_TEST7 /* Host to Host */

    for(size_t stream_size  = 250000; stream_size <= 1024000000; stream_size *=2) { 

	    printf("TEST OF STREAM HOST TO HOST  HOST TO DEVICE HARDCODED \n"); 
	    trans_count = 2;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
	    src = 0; dst = 1;
        CUDA_ERROR(cudaSetDevice(src),"cudaSetDevice");
        CUDA_ERROR(cudaMemcpyAsync(test1[dst],test1[src],stream_size,cudaMemcpyDeviceToDevice),"cudaMemcpyPerrAsync");
        CUDA_ERROR(cudaMemcpyAsync(test1_host[0],test1[src],stream_size,cudaMemcpyDeviceToHost),"cudaMemcpyPerrAsync");
        
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSync");
        }
        time__omp += omp_get_wtime();
        fprintf(stdout,"time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);

    }
/*  END OF THIS THIST */
#endif


#if STREAM_TEST7_MCL /* Host To Host Host To Device*/
{    
    mcl_pttr_t *pt1,*pt2;
    err  = mcl_alloc_pttr(&pt1,0);
    
    for(size_t stream_size  = 250000; stream_size<= 1024000000; stream_size*=2 ) { 
        err  = mcl_alloc_pttr(&pt1,0);
        err = mcl_add_path(test1[1],test1[0],stream_size,pt1);
        err = mcl_add_path(test1_host[0],test1[0],stream_size,pt1);

	    printf("TEST STREAM HOST TO HOST HOST TO DEVICE MCL  0-1 0-H \n"); 
    	trans_count = 2;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
        err = mcl_sync_excomm(pt1);
	                
        time__omp += omp_get_wtime();
        fprintf(stdout,"MCL:time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);
    
        err = mcl_rmv_path(test1_host[0],test1[0],stream_size,pt1);
    }

}
#endif

#if STREAM_TEST8 /* Host to Host */

    for(size_t stream_size  = 250000; stream_size <= 1024000000; stream_size *=2) { 

	    printf("TEST OF STREAM HOST TO HOST  HOST TO DEVICE HARDCODED \n"); 
	    trans_count = 2;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
	    src = 0; dst = 1;
        CUDA_ERROR(cudaSetDevice(src),"cudaSetDevice");
        CUDA_ERROR(cudaMemcpyAsync(test1[dst],test1[src],stream_size,cudaMemcpyDeviceToDevice,streams1[src]),"cudaMemcpyPerrAsync");
        CUDA_ERROR(cudaMemcpyAsync(test1_host[0],test1[dst],stream_size,cudaMemcpyDeviceToHost,streams1[dst]),"cudaMemcpyPerrAsync");
        
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSync");
        }
        time__omp += omp_get_wtime();
        fprintf(stdout,"time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);

    }
/*  END OF THIS THIST */
#endif


#if STREAM_TEST8_MCL /* Host To Host Host To Device*/
{    
    mcl_pttr_t *pt1,*pt2;
    err  = mcl_alloc_pttr(&pt1,0);
    
    for(size_t stream_size  = 250000; stream_size<= 1024000000; stream_size*=2 ) { 
        err  = mcl_alloc_pttr(&pt1,0);
        err = mcl_add_path(test1[1],test1[0],stream_size,pt1);
        err = mcl_add_path(test1_host[0],test1[1],stream_size,pt1);

	    printf("TEST STREAM HOST TO HOST HOST TO DEVICE MCL  0-1 1-H \n"); 
    	trans_count = 2;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
        err = mcl_sync_excomm(pt1);
	                
        time__omp += omp_get_wtime();
        fprintf(stdout,"MCL:time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);
    
        err = mcl_rmv_path(test1_host[0],test1[0],stream_size,pt1);
    }

}
#endif
#if STREAM_TEST9 /* Host to Host */

    for(size_t stream_size  = 250000; stream_size <= 1024000000; stream_size *=2) { 

	    printf("TEST OF STREAM HOST TO HOST  HOST TO DEVICE DEVICE HOST HARDCODED \n"); 
	    trans_count = 3;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
	    src = 0; dst = 1;
        CUDA_ERROR(cudaSetDevice(src),"cudaSetDevice");
        CUDA_ERROR(cudaMemcpyAsync(test1[dst],test1[src],stream_size,cudaMemcpyDeviceToDevice,streams1[src]),"cudaMemcpyPerrAsync");
        CUDA_ERROR(cudaMemcpyAsync(test1_host[0],test1[dst],stream_size,cudaMemcpyDeviceToHost,streams1[dst]),"cudaMemcpyPerrAsync");
       CUDA_ERROR(cudaMemcpyAsync(test1[0],test1_host[1],stream_size,cudaMemcpyDeviceToHost,streams2[dst]),"cudaMemcpyPerrAsync");
 
        for(int i = 0; i<GPU_COUNT; i++) {
            CUDA_ERROR(cudaSetDevice(i),"cudaSetDevice");
            CUDA_ERROR(cudaDeviceSynchronize(),"cudaDeviceSync");
        }
        time__omp += omp_get_wtime();
        fprintf(stdout,"time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);

    }
/*  END OF THIS THIST */
#endif


#if STREAM_TEST9_MCL /* Host To Host Host To Device*/
{    
    mcl_pttr_t *pt1,*pt2;
    err  = mcl_alloc_pttr(&pt1,0);
    
    for(size_t stream_size  = 250000; stream_size<= 1024000000; stream_size*=2 ) { 
        err  = mcl_alloc_pttr(&pt1,0);
        err = mcl_add_path(test1[1],test1[0],stream_size,pt1);
        err = mcl_add_path(test1_host[0],test1[1],stream_size,pt1);
        err = mcl_add_path(test1[0],test1_host[1],stream_size,pt1);

	    printf("TEST STREAM HOST TO HOST HOST TO DEVICE  HOST TO DEVICE MCL  0-1 1-H H-0 \n"); 
    	trans_count = 3;
        time__omp = 0.0;
        time__omp -= omp_get_wtime();        
        err = mcl_sync_excomm(pt1);
	                
        time__omp += omp_get_wtime();
        fprintf(stdout,"MCL:time: %f,stream size : %f MB , Total transfer on fabric: %f GB/s \n",time__omp,(stream_size)*(1e-6),(trans_count*(stream_size)/time__omp)*1e-9);
    
        err = mcl_rmv_path(test1_host[0],test1[0],stream_size,pt1);
    }

}
#endif


  }while(0);//For running multiple times



}


