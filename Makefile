CC =  gcc
CFLAG =  -O0 -Wall -g  -pthread -fms-extensions -std=gnu99  -I$(CUDA_INCLUDE)  #-Wextra # -g 
#TODO: check out pthread options
NVCC = nvcc
CUDACFLAGS = -fopenmp  -lcudart -gencode=arch=compute_35,code=compute_35 -rdc=true -O3 -m64   -Xcompiler  -Xcompiler -fno-strict-aliasing -Xcompiler -funroll-loops -Xcompiler  -Xptxas -v
CUDA_HOME  = /usr/local/cuda-8.0/
CUDA_INCLUDE = /usr/local/cuda-8.0/include
CUDA_LIB     = /usr/local/cuda-8.0/lib64

LINKER = gcc -o 
LFLAG =  -Wall -lpthread -L$(CUDA_LIB)  -lcudart #-lnuma
NVFLAG = -Wallx
NVLFLAG =  -lgomp

binaries =  mcl_test
SRC = 


RM = rm -f

VALGRIND_SUPP = --suppressions=cuda.supp
VALGRIND_FLAG = $(VALGRIND_SUPP)
VALGRIND = valgrind   $(VALGRIND_FLAG)


.PHONY: all
all: $(binaries)

%.o: %.cu
	$(NVCC) $< $(CUDACFLAG) -c -o $@

%.o: %.c
	$(CC) $< $(CFLAG) -c -o $@

.PHONY: debug
debug: CC += -DDEBUG
debug: all


mcl_test: mcl.o  all_cmpl.o cmpl.o  round_rob_cmpl.o left_right_cmpl.o brdc_cmpl.o pair_cmpl.o cmnd_engine.o mcl_test.o 
	$(CC)  $^ $(LFLAG) -o $@ 

test_all_cmpl: all_cmpl.c 
	$(CC)  all_cmpl.c -DTEST_ALL_CMPL $(LFLAG) -o $@

.PHONY: lib
lib: mcl.o all_cmpl.o cmpl.o cmnd_engine.o
	ar -cvq libmcl.a mcl.o all_cmpl.o cmpl.o cmnd_engine.o

#.PHONY: all_cmpl_test
#test_all_cmpl: all 
#	./all_cmpl_test 

#Runs the unit tests
.PHONY: test
test:	all
	$(VALGRIND) ./mcl_test 2>&1 | grep -v  -E 'Warning: noted but unhandled ioctl|This could cause spurious value errors to appear.|See README_MISSING_SYSCALL_OR_IOCTL'
#Runs the unit tests
.PHONY: test_nvprof
test_nvprof:	
	nvprof --print-gpu-trace ./mcl_test 

.PHONY: clean
clean:
	$(RM) $(binaries) 
	$(RM) *~
	$(RM) *.o

.PHONY: prof
prof:
	nvprof --print-gpu-trace ./mcl_test
.PHONY: stream_test

stream_test: mcl.o  all_cmpl.o round_rob_cmpl.o left_right_cmpl.o brdc_cmpl.o pair_cmpl.o cmpl.o cmnd_engine.o stream_test.o
	$(NVCC)  $^ $(NVLFLAG) -o $@ 

.PHONY: stream
stream:
	./stream_test
.PHONY: stream_prof
stream_prof:
	nvprof --print-gpu-trace ./stream_test

.PHONY: panf
panf: mcl.o  all_cmpl.o round_rob_cmpl.o left_right_cmpl.o brdc_cmpl.o pair_cmpl.o cmpl.o cmnd_engine.o panf3d_mcl.o
	$(NVCC) $^ -lgomp   -o $@
.PHONY: run_panf
run_panf:
	./panf 100

.PHONY: tarball
tarball: clean
	tar -cvf mcl.tar *

.PHONY: info
info: 
	./info.sh
