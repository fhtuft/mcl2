/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

#ifndef MCL_H
#define MCL_H

#include "mcl_err.h"
#include "comm_cmpl.h"
#include "pttr_opt.h"

typedef  struct comm_pttr mcl_pttr_t;
typedef  struct comm_pttr mcl_const_pttr_t;

mcl_res_t mcl_init(void);
mcl_res_t mcl_finalize(void);



mcl_res_t mcl_alloc_pttr(mcl_pttr_t **pt,uint32_t opt);
mcl_res_t mcl_free_pttr(mcl_pttr_t *comm);

mcl_res_t mcl_add_path(void *dst,void *src,size_t size,mcl_pttr_t *pt);
mcl_res_t mcl_rmv_path(void *dst,void *src,size_t size,mcl_pttr_t *pt);//TODO: Why size?



/* Prededefined patterns */
mcl_res_t mcl_alloc_bcast(void *src,size_t size,mcl_pttr_t **pt,const int dst_cnt,...);
mcl_res_t mcl_alloc_gather(void *dst,size_t size,mcl_pttr_t **pt,const int src_cnt,...);
mcl_res_t mcl_alloc_scatter(void *src,size_t size,mcl_pttr_t **pt,const int dst_cnt,...);
mcl_res_t mcl_alloc_1dim_bexch(size_t size,mcl_pttr_t **pt,int pair_cnt,...);
mcl_res_t mcl_alloc_2dim_bexch(size_t size,mcl_pttr_t **pt,int x_pair_cnt,int y_pair_cnt,...);
//mcl_res_t mcl_alloc_3dim_bexch(size_t size,mcl_pttr_t **pt,int x_pair_cnt,int y_pair_cnt,int z_pair_cnt,...);


/* Execute the communication pattern */
mcl_res_t mcl_sync_excomm(mcl_pttr_t *pt);
mcl_res_t mcl_sync_omp_excomm(mcl_pttr_t *pt);

/* Changes the default compiler*/
mcl_res_t mcl_cng_comm_cmpl(mcl_comm_cmpl_t cc);
mcl_res_t mcl_add_comm_cmpl(mcl_comm_cmpl_t cc, mcl_pttr_t *pt);
/* Add own comm compiler function */
//mcl_res_t mcl_add_raw_comm_cmpl(void *fp,mcl_pattr_t *pt);

#endif 
