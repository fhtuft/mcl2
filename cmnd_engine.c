/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/


#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <stdbool.h>

#include "cmnd_engine.h"
#include "mcl_err.h"
#include "pttr.h"
#include "debug.h"
#include "common.h"


const char * print_cmnd(const cmnd_eng_cmnd_t type)  {
/* to be used for debuging, therefor the "intelignet" names"*/
     switch(type) {
            case CMND_ENG_CMND_AP2P:          return "ap2p";
            case CMND_ENG_CMND_AP2P_REVENT:   return "ap2p revent";
            case CMND_ENG_CMND_AP2P_S:        return "ap2p s";
            case CMND_ENG_CMND_AP2P_REVENT_S: return "ap2p revent s";
            case CMND_ENG_CMND_REVENT:        return "rvent";
            case CMND_ENG_CMND_REVNET_S:      return "revent s";
            case CMND_ENG_CMND_WEVENT:        return "wenvent";
            case CMND_ENG_CMND_MEMCPY_HD:     return "memcpy hd";
            case CMND_ENG_CMND_MEMCPY_DH:     return "memcpy dh";
            case CMND_ENG_CMND_AMEMCPY_HD:    return "amemcpy hd";
            case CMND_ENG_CMND_AMEMCPY_DH:    return "amemcpy dh";
            case CMND_ENG_CMND_AMEMCPY_HD_S:  return "amemcpy_hd_s";
            case CMND_ENG_CMND_AMEMCPY_DH_S:  return "amemcpy_dh_s";
            case CMND_ENG_CMND_DEVSYNC:       return "devsync";
            case CMND_ENG_CMND_HALT:          return "halt";
            default:                          return "not a command";
        }


}


void debug_print_cmnds(struct comm_pttr *pt) {
    
    struct cmnd *prg = pt->prg;  
    
    for(int count = 0; prg->type != CMND_ENG_CMND_HALT; prg = prg->nxt, count++) 
       fprintf(stderr,"debug_print of program: %s:count:%d\n",print_cmnd(prg->type),count); 
     
}

mcl_res_t run_cmnd_engine(struct comm_pttr *pt) {
  DEBUG_PRINT("Entry command engine\n"); 
static const void *jmp_tbl[] = {
    &&SR_AP2P,
    &&SR_AP2P_REVENT,
    &&SR_AP2P_S,
    &&SR_AP2P_REVENT_S,
    &&SR_REVENT,
    &&SR_REVENT_S,
    &&SR_WEVENT,
    &&SR_MEMCPY_HD,
    &&SR_MEMCPY_DH,
    &&SR_AMEMCPY_HD,
    &&SR_AMEMCPY_DH,
    &&SR_AMEMCPY_HD_S,
    &&SR_AMEMCPY_DH_S,
    &&SR_DEVSYNC, 
    &&SR_HALT
};


_Static_assert(sizeof(jmp_tbl)/sizeof(*jmp_tbl) == CMND_ENG_CMND_CAP,"missmatch on count of jmp_tbl entrys and count of cmnd_eng_cmnd_t  elements");

    /* Error varibales */
    mcl_res_t err = MCL_SUCCESS;  
    cudaError_t cuda_err = cudaSuccess;
 
    /* Init the commands  */
    struct cmnd cap;/*Cap for top of the cmnds, used to do the program simpler*/ 
    struct cmnd *prg = &cap; 
    prg->nxt = pt->prg;

LOOP_ON_JMPT:
    prg = prg->nxt;
    assert(prg->type != CMND_ENG_CMND_CAP);
    DEBUG_PRINT("prg->type:%s\n",print_cmnd(prg->type));
    DEBUG_PRINT("prg->type:%d\n",(int)prg->type);
    DEBUG_PRINT("jmp_tbl[prg->type]:%p :%p\n",jmp_tbl[prg->type],&&SR_AP2P_S);
    goto *jmp_tbl[prg->type];//(jmp_tbl[prg->type]);
SR_AP2P:        /* async p2p cpy*/
    /* http://stackoverflow.com/questions/22736159/what-device-number-should-i-use-0-or-1-to-copy-p2p-gpu0-gpu1 */
    /* setDevice to where stream created, for best performence use src stream*/
    DEBUG_PRINT("SR_AP2P\n");
    {
        
        struct cmnd_eng_cmnd_ap2p *cmnd = &prg->ap2p;
        int * dst_device = cmnd->dst_device;
        int *src_device = cmnd->src_device;
        size_t *sizeof_comms = cmnd->sizeof_comms;
        void **src = cmnd->src;
        void **dst = cmnd->dst;
        cudaStream_t * streams = cmnd->streams;         

        for(int i  =0 ; i < cmnd->count; i++) {
            cuda_err = cudaSetDevice(src_device[i]);
            cuda_err = cudaMemcpyPeerAsync(dst[i],dst_device[i],src[i],src_device[i],sizeof_comms[i],streams[i]);
        }

        goto LOOP_ON_JMPT; 

    }
SR_AP2P_REVENT: /* async p2p cpy and recored event in streams */
    DEBUG_PRINT("SR_AP2P_REVENT\n");
    {
        struct cmnd_eng_cmnd_ap2p_revent *cmnd = &prg->ap2p_revent;
        int *dst_device = cmnd->dst_device;
        int *src_device = cmnd->src_device;
        size_t *sizeof_comms = cmnd->sizeof_comms; //Is array
        void ** src = cmnd->src; //Is array
        void ** dst = cmnd->dst; //Is array
        cudaStream_t *streams = cmnd->streams;         
        cudaEvent_t *events = cmnd->events;

        for(int i  =0 ; i < cmnd->count; i++) {
            cuda_err = cudaSetDevice(src_device[i]);
            cuda_err = cudaMemcpyPeerAsync(dst[i],dst_device[i],src[i],src_device[i],sizeof_comms[i],streams[i]);
            cuda_err = cudaEventRecord(events[i],streams[i]);

        }

        goto LOOP_ON_JMPT;
   
     }
SR_AP2P_S:
    DEBUG_PRINT("SR_AP2P_S\n");
    {
        
        struct cmnd_eng_cmnd_ap2p_s *cmnd = &prg->ap2p_s;
        int dst_device = cmnd->dst_device;
        int src_device = cmnd->src_device;
        size_t sizeof_comms = cmnd->sizeof_comms;
        void *src = cmnd->src;
        void *dst = cmnd->dst;
        cudaStream_t stream = cmnd->stream;
        DEBUG_PRINT("dst_device:%d:src_device:%d:sizeof_comms:%d:src%p:dst:%p:stram:%d\n",dst_device,src_device,sizeof_comms,src,dst,(int)stream);
        cuda_err = cudaSetDevice(src_device);
        CUDA_ERROR(cuda_err,"cudaSetDevice");
        cuda_err = cudaMemcpyPeerAsync(dst,dst_device,src,src_device,sizeof_comms,stream);
        
        CUDA_ERROR(cuda_err,"cudaMemcpyPeerAsync"); 
    
        goto LOOP_ON_JMPT;
    }
SR_AP2P_REVENT_S:
    DEBUG_PRINT("SR_AP2P_REVENT_S\n");
    {
        struct cmnd_eng_cmnd_ap2p_revent_s *cmnd = &prg->ap2p_revent_s;
        int dst_device = cmnd->dst_device;
        int src_device = cmnd->src_device;
        size_t sizeof_comms = cmnd->sizeof_comms;
        void *src = cmnd->src;
        void *dst = cmnd->dst;
        cudaStream_t stream = cmnd->stream;
        cudaEvent_t event = cmnd->event; //TODO:  use this ???
        cuda_err = cudaSetDevice(src_device);
        CUDA_ERROR(cuda_err,"cudaSetDevice");
        cuda_err = cudaMemcpyPeerAsync(dst,dst_device,src,src_device,sizeof_comms,stream);
        CUDA_ERROR(cuda_err,"cudaMemcpyPeerAsync"); 
        cuda_err = cudaEventRecord(event,stream);
        CUDA_ERROR(cuda_err,"cudaEventRecord");
    
    
        goto LOOP_ON_JMPT;
    }



SR_REVENT:      /* record event in stream */
    DEBUG_PRINT("SR_REVENT");
    {
        struct cmnd_eng_cmnd_revent *cmnd = &prg->revent;

        int count = cmnd->count;
        int *device = cmnd->device;
        cudaStream_t *streams = cmnd->streams;
        cudaEvent_t *events = cmnd->events;
        for(int i = 0; i< count; i++) {
            cuda_err = cudaSetDevice(device[i]);
            cuda_err = cudaEventRecord(events[i],streams[i]);
        }
    
    
    }
    goto LOOP_ON_JMPT;
SR_REVENT_S:      /* record event in stream */
    {
        struct cmnd_eng_cmnd_revent_s *cmnd = &prg->revent_s;

        int device = cmnd->device;
        cudaStream_t streams = cmnd->stream;
        cudaEvent_t events = cmnd->event;
        cuda_err = cudaSetDevice(device);
        CUDA_ERROR(cuda_err,"cudaSetDevice");
        cuda_err = cudaEventRecord(events,streams);
        CUDA_ERROR(cuda_err,"cudaEventRecord");
     
    }
    goto LOOP_ON_JMPT;



SR_WEVENT:      /* wait for events */
    {//TODO do something whit the wevent revent ap..event etc 
        /*
            Or do this with waitEvent ?? 
        */
        struct cmnd_eng_cmnd_wevent *cmnd =  &prg->wevent;       
        size_t count = cmnd->count;
        bool *has_occurred = cmnd->has_occurred;
        cudaEvent_t *events = cmnd->events;
 
        //cudaError_t event_res = cudaSuccess; //Not used
        for(int occurred_count = 0 ; occurred_count != count;) {
            for(int i =0; i<count; i++) {    
                if(has_occurred[i])
                    continue;
                switch(cudaEventQuery(events[i])) {
                    case cudaSuccess:
                        has_occurred[i] = true;
                        occurred_count++;
                        break; 
                    case cudaErrorNotReady:
                        break;
                    default:
                        /* Handel error */
                        ; //TODO SOMETHING HER
                }
            }
        }

        goto LOOP_ON_JMPT;
    }
SR_MEMCPY_HD:   /* memcpy host to device */

    {
        struct cmnd_eng_cmnd_memcpy_hd *cmnd = &prg->memcpy_hd;
        size_t count = cmnd->count;
        size_t * sizeof_comms = cmnd->sizeof_comms;
        int * device = cmnd->device;
        void ** src = cmnd->src;
        void ** dst = cmnd->dst;
        
        for(int i = 0; i< count; i++) {
            cuda_err = cudaSetDevice(device[i]);
            cuda_err = cudaMemcpy(dst[i],src[i],sizeof_comms[i],cudaMemcpyHostToDevice);  
        }
    }
    goto LOOP_ON_JMPT;
SR_MEMCPY_DH:   /* memcpy device to host */
     {
        struct cmnd_eng_cmnd_memcpy_hd *cmnd = &prg->memcpy_hd;
        size_t count = cmnd->count;
        size_t *sizeof_comms = cmnd->sizeof_comms;
        void **src = cmnd->src;
        void **dst = cmnd->dst;
        int *device = cmnd->device;
        for(int i = 0; i< count; i++) {
            cuda_err = cudaSetDevice(device[i]);
            cuda_err = cudaMemcpy(dst[i],src[i],sizeof_comms[i],cudaMemcpyDeviceToHost);  
        }
    }

    goto LOOP_ON_JMPT;
SR_AMEMCPY_HD:  /* async memcpy host to device*/
    goto LOOP_ON_JMPT;
SR_AMEMCPY_DH:  /* async memcpy device to host*/
    goto LOOP_ON_JMPT;
SR_AMEMCPY_HD_S:  /* async memcpy host to device singel*/
        {
            struct cmnd_eng_cmnd_amemcpy_hd_s *cmnd = &prg->amemcpy_hd_s;
        
            int device = cmnd->device; 
            size_t sizeof_comm = cmnd->sizeof_comm;
            void *src = cmnd->src;
            void *dst = cmnd->dst;
            cudaStream_t stream = cmnd->stream;
            cuda_err = cudaSetDevice(device);
            if(cuda_err != cudaSuccess) goto ERROR_CUDA;
            cuda_err = cudaMemcpyAsync(dst,src,sizeof_comm,cudaMemcpyHostToDevice,stream); 
            if(cuda_err != cudaSuccess) goto ERROR_CUDA;

        }
    goto LOOP_ON_JMPT;
SR_AMEMCPY_DH_S:  /* async memcpy device to host singel*/
    {
        struct cmnd_eng_cmnd_amemcpy_dh_s *cmnd = &prg->amemcpy_dh_s;

        int device = cmnd->device; 
        size_t sizeof_comm = cmnd->sizeof_comm;
        void *src = cmnd->src;
        void *dst = cmnd->dst;
        cudaStream_t stream = cmnd->stream;
        cuda_err = cudaSetDevice(device);
        if(cuda_err != cudaSuccess) goto ERROR_CUDA;
        cuda_err = cudaMemcpyAsync(dst,src,sizeof_comm,cudaMemcpyDeviceToHost,stream); 
        if(cuda_err != cudaSuccess) goto ERROR_CUDA;

    }
    goto LOOP_ON_JMPT;
SR_DEVSYNC:
    {   
        struct cmnd_eng_cmnd_devsync *cmnd = &prg->devsync;
        int count   = cmnd->count;
        int *devices = cmnd->devices;
    
        for(int i = 0; i<count; i++) {
            cuda_err = cudaSetDevice(devices[i]);
            if(cuda_err != cudaSuccess) goto ERROR_CUDA;
            cuda_err = cudaDeviceSynchronize();
            if(cuda_err != cudaSuccess) goto ERROR_CUDA;
        }

    }   
    goto LOOP_ON_JMPT;

SR_HALT:        /* halt and return */
    DEBUG_PRINT("SR_HALT\n");
    {
        //struct cmnd_eng_cmnd_halt *cmnd = &prg->halt;
        cap.nxt = NULL; //TODO do this ??
        return MCL_SUCCESS;
    }

assert(("Program should never comme her",0));
ERROR_CUDA:
    //TODO print cuda error
    err = MCL_CUDA_ERROR;
RETURN_ERROR:
    cap.nxt = NULL;
    return err;
}
