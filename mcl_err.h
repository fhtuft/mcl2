/*
MIT License

Copyright (c) 2017 Finn-Haakon Ellingrud Tuft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

#ifndef MCL_ERR_H
#define MCL_ERR_H

typedef enum {
    MCL_SUCCESS                     =  0, 
    MCL_ERROR                       = -1,
    MCL_CUDA_ERROR                  = -2,
    MCL_COMM_FULL                   = -3,
    MCL_STACK_OVERFLOW              = -4,
    MCL_PCI_TOPO_FILE_ASSERT        = -5,
    MCL_INVALID_VALUE_PCI_TOPO_FILE = -6,
    MCL_UNKNOWN_PATH_SIGNATURE      = -7,
    MCL_NOT_IMPLEMENTED             = -8
}mcl_res_t;

static inline const char * mcl_get_res_str(mcl_res_t res) {
    switch(res) {
        case MCL_SUCCESS              : return "mcl succes";
        case MCL_ERROR                : return "mcl error";
        case MCL_CUDA_ERROR           : return "cuda error";
        case MCL_COMM_FULL            : return "to many memory addresses";
        case MCL_STACK_OVERFLOW       : return "stack overflow in internal stack";
        case MCL_PCI_TOPO_FILE_ASSERT : return "assert on value in pci topo file";
        case MCL_INVALID_VALUE_PCI_TOPO_FILE : return "invalid value pci topo file";
        case MCL_UNKNOWN_PATH_SIGNATURE : return "unkown path signature";
        case MCL_NOT_IMPLEMENTED       : return "Not implemented";
    }
    return "unknown error";
};

static inline int _mcl_error_print(const mcl_res_t res) {
    if(res) {
        fprintf(stderr,"%s\n",mcl_get_res_str(res));
        return 1;
    }
    return 0;
}

#define MCL_ERROR_PRINT(err) _mcl_error_print((err))
#define MCL_ERROR_EXIT(err)  if(_mcl_error_print((err))){exit(EXIT_FAILURE);}


#endif
